---
title: Imperativ programmering (C) Modul 9
---

# Text- och filhantering, void-pekare, piloperatorn samt dynamiskt minne #

> Testfrågor
>
> Du är redo för mästarprovet först när du förstår och kan svara på
> följande frågor.


> 1. Skriv en funktion som konkatenerar alla kommandoradsargument till
> en enda lång sträng som sedan skrivs ut. Tecknet | skall användas
> för att skilja argumenten åt.  Använd funktionerna strlen och
> strncat.

``` c
void cat_args(int argc, const char *argv[])
{
    const int n = 10;
    char str[n];
    const char div[] = "|";
    str[0] = 0;

    for (int i = 1;
         i < argc && strlen(str) + strlen(argv[i]) < n;
         i++) {
        strncat(str, argv[i], strlen(argv[i]));
        printf("str is %d bytes long\n", (int) strlen(str));
        if (i < argc - 1) {
            strncat(str, div, strlen(div));
        }
    }
    printf("%s\n", str);
}
```

> 2. Skriv en funktion som ändrar alla gemener i det engelska
> alfabetet (a-z) till versaler (A-Z). Använd funktionen toupper.

``` c
void upcase_string(int n, char str[])
{
    int i = 0;
    while (str[i] != 0 && i < n) {
        str[i] = toupper(str[i]);
        i++;
    }
}
```

> 3. Skriv en funktion som räknar hur många rader i en textfil som är
> längre än 80 tecken. Använd funktionen fgetc. Tänk på att ta hänsyn
> till radbrytningsformatet.

``` c
void count_long_lines(const char filename[])
{
    FILE *fp = fopen(filename, "r");
    int long_lines = 0;
    int c, i = 0;
    if (fp != NULL) {
        do {
            c = fgetc(fp);
            if ((char) c == '\n' || (char) c == '\r') {
                long_lines += (i > 80) ? 1 : 0;
                i = 0;
                }
            else {
            i++;
            }
        } while (c != EOF);
    printf("%d lines are longer than 80 characters.\n", long_lines);
    fclose(fp);
    }
}
```

> 4. Skriv en funktion som läser in en textfil med personnummer och skriver ut hur
> många nummer som hör till en man respektive en kvinna. (Den tredje siffran i de
> fyra sista siffrorna är jämn för kvinnor och udda för män.) Personnumren anges en
> per rad på formatet yymmdd-nnnn. Använd funktionen fscanf.

``` c
void count_persons(const char filename[])
{
    FILE *fp = fopen(filename, "r");

    if (fp != NULL) {
        int date, four;
        int count_m = 0, count_w = 0;
        while (EOF != fscanf(fp, "%d-%d", &date, &four)) {
            ((four/10) % 2 ) ? count_m++ : count_w++;
        }
        fclose(fp);
        printf("%d women and %d men\n", count_w, count_m);
    }
}
```

> 5. Skriv en funktion som skriver ut storleken på en heltalsarray
> följt av innehållet till en textfil. Skilj talen åt med ett
> mellanrum.

``` c
void write_array_to_file(const char filename[], int n, const int a[])
{
    FILE *fp = fopen(filename, "w");
    if (fp != NULL) {
        fprintf(fp, "%d ", n);
        for (int i = 0; i < n; i++) {
            fprintf(fp, "%d ", a[i]);
        }
    fclose(fp);
    }
}
```

> 6. Skriv en funktion som läser in en heltalsarray från en
> textfil. Antag att det första talet anger arrayens storlek och att
> talen skiljs åt av ett mellanrum.

``` c
void read_array_from_file(const char filename[], int *n, int arr[])
{
    FILE *fp = fopen(filename, "r");
    if (fp != NULL) {
        int nr_of_elements;
        fscanf(fp, "%d", &nr_of_elements);
        *n = nr_of_elements;
        for (int i = 0; i < nr_of_elements && i < MAX_SIZE; i++) {
            fscanf(fp, "%d", &arr[i]);
        }
    fclose(fp);
    }
}
```

> 7. Dela upp ett program med flera funktioner till en header-fil och
> en source-fil. In- kludera header-filen i source-filen. Se till att
> förhindra att dubbelinkludering kan ske.

[code/M9/test_question_7.h](code/M9/test_question_7.h)

[code/M9/test_question_7.c](code/M9/test_question_7.c)


> 8. Använd malloc för att allokera
>
> • en int

``` c
int *pn = (int*) malloc(sizeof(int));
```

> • en array av 15 st double

``` c
double *arr = (double*) malloc(15 * sizeof(double));
```

> • en array av 3 st `struct s { int n; char *p; }`

``` c
typedef struct {
    int n;
    char *p;
    } s;

s *s = malloc(3 * sizeof(s));
```

> 9. Gör om föregående uppgift men använd calloc istället för malloc.

``` c
int *pn = (int*) calloc(1, sizeof(int));
double *arr = (double*) calloc(15, sizeof(double));
s *x = calloc(3, sizeof(s));
```

> 10. Definiera en funktion, `int *random_array(int n)`, som
> returnerar en dynamiskt allokerad array av heltal av en given
> storlek med slumpade värden mellan 0 och 99.

``` c
int *random_array(int n)
{
    int *a = (int*) calloc(n, sizeof(int));
    srand(time(NULL));
    for (int i = 0; i < n; i++) {
        a[i] = rand() % 100;
    }

    return a;
}
```

> 11. Definiera en funktion, `int *copy_array(int n, const int a[n])`,
> som returnerar en kopia av en array.

``` c
int *copy_array(int n, const int a[n])
{
    int *b = (int) calloc(n, sizeof(int));

    for (int i = 0; i < n; i++) {
        b[i] = a[i];
    }

    return b;
}
```

> 12. Definiera en funktion, `char *copy_string(const char *s)`, som
> returnerar en kopia av en sträng.

``` c
char *copy_string(const char *s)
{
    int n = 0;
    while (s[n++] != 0);

    char *rv = calloc(n, sizeof(char));
    for (int i = 0; i <= n; i++) {
        rv[i] = s[i];
    }

    return rv;
}
```

> 13. Definiera en funktion, `char *read_line(void)`, som läser in en
> rad från användaren av programmet. En rad avslutas med `\n`. Använd
> `getchar()` för att läsa ett tecken åt gången. Använd först malloc
> (eller calloc) och free för den dynamiska
> minneshanteringen. Förenkla sedan lösningen genom att använda
> realloc.

Lösning med `malloc()` och `free()`:

``` c
char *read_line(void)
{
    char *str = NULL, c;
    int n = 0;

    do {
        char *tmp = malloc((n + 1) * sizeof(char));
        c = (char) getchar();
        tmp[n] = c;
        for (int i = 0; i < n; i++) {
            tmp[i] = str[i];
            }
        n++;
        free(str);
        str = tmp;
        } while (c != 0xa);

    // terminate string
    str[n-1] = 0;

    return str;
}
```

Enklare lösning med `realloc()`:

``` c
char *read_line(void)
{
    char *str = NULL, c;
    int n = 0;

    do {
        c = (char) getchar();
        str = realloc(str, (n + 1) * sizeof(char));
        str[n] = c;
        n++;
    } while (c != 0xa);

    // terminate string
    str[n - 1] = 0;

    return str;
}
```

> 14. Definiera en funktion, `int *read_array(const char *file)`, som
> läser in en array av heltal från en textfil. Talen i filen separeras
> med mellanrum. Det första talet i filen anger det totala antalet tal
> i arrayen. Därefter följer arrayens innehåll i ordning.

``` c
int *read_array(const char *file)
{
    int *a, n;
    FILE *fp = fopen(file, "r");

    if (fp == NULL) {
        return 1;
    }

    fscanf(fp, "%d", &n);
    a = calloc(n, sizeof(int));
    for (int i = 0; i < n; i++) {
        fscanf(fp, "%d", &a[i]);
    }
    fclose(fp);

    return a;
}
```

> 15. Definiera en funktion, `int *read_matrix(const char *file)`, som
> läser in en matris av heltal från en textfil. Talen på samma rad i
> filen separeras med mellanrum.  Den första raden i filen anger hur
> många rader och kolumner med data (heltal) det finns i filen, det
> första talet på raden anger antal rader med data och det andra talet
> anger antal kolumner med data. Därefter följer rad för rad med
> (kolumner antal) data.

Det måste vara en `int **function()`:

``` c
int *read_matrix(const char *file)
{
    int **m;
    FILE *fp = fopen(file, "r");
    int rows, cols;

    if (fp == NULL) {
        return NULL;
    }

    fscanf(fp, "%d %d", &rows, &cols);

    m = (int**) calloc(rows, sizeof(int*));
    for (int i = 0; i < rows; i++) {
        m[i] = (int*) calloc(cols, sizeof(int));
        for (int j = 0; j < cols; j++) {
            fscanf(fp, "%d", &m[i][j]);
        }
    }

    fclose(fp);

    return m;
}
```

> 16. Vad skrivs ut av följande kod?
>
>         int x = 1234;
>         char str [16];
>         sprintf(str , "%.3f", (double) x);
>         printf("%c %d\n", str[6], (int)strlen(str ));

`str` kommer att innehålla `1234.000\0`. `strlen("1234.000")`
blir 8. Alltså:

`0 8` och en radbrytning.


> 17. Gör följande:
>
> * Skriv en funktion `double *alloc_array(int length);` som allokerar
>   en double-array av längd length och returnerar en pekare till
>   arrayen.

``` c
double *alloc_array(int length)
{
    return calloc(length, sizeof(double));
}
```

> * Skriv en funktion `void sum(double *array, int length, double
>   *sum_numbers, double *sum_squares);` som lagrar summan av alla tal
>   bland de length första elementen i array i variablen som
>   `sum_numbers` pekar på, och lagrar summan av alla kvadrater av
>   alla tal bland de length första elementen i array i variablen som
>   `sum_squares` pekar på. Kvadraten av ett tal är talet gånger sig
>   själv.

``` c
void sum(double *array, int length,
         double *sum_numbers, double *sum_squares)
{
    *sum_numbers = 0;
    *sum_squares = 0;
    for (int i = 0; i < length; i++) {
        sum_numbers += array[i];
        sum_squares += array[i] * array[i];
    }
}
```



> * Skriv en funktion som lämnar tillbaka det minne som allokerats med
> hjälp av `alloc_array`.

``` c
void free_array(double *a)
{
    free(a);
}
```

> 18. Betrakta följande kod:


``` c
#include <stdio.h>

int main(int argc , char *argv [])
{

    char line [300];
    FILE *fp1 , *fp2;
    int x = 1;

    fp1 = fopen(argv [1], "r");
    fp2 = fopen(argv [2], "w");

    while (fgets(line , 300, fp1) != NULL) {
        fprintf(fp2 , "%-5d %s", x, line );
        x++;
    }

return 0;
}
```

> * Beskriv vad koden gör. (Tips: Beskriv inte rad för rad vad som
> händer utan vad man vill uppnå genom att exekvera koden.)

Koden öppnar en första fil vars namn skickas med som första argument
på kommandraden för läsning. Därefter öppnas en andra fil vars namn
skickas med som andra argument på kommandraden för skrivning. I en
loop läser programmet in en rad i taget från fil 1 tills den kommer
till slutet av fil 1. Varje rad som lästs in skrivs till fil 2
tillsammans med ett radnummer.

> * Föreslå två förbättringar av koden.

* Kontrollera att det verkligen finns två kommandradsargument (t ex
med `if (argc >= 3) {...`).
* Kontrollerar att filerna kinde öppnas genom att testa om filpekarna
  `fp1` och `fp2` skiljer sig från `NULL`.
* använda en konstant för värdet 300 (längd av strängen) som används
  på två ställen, för att förebygga att man missar att ändra på båda
  ställen när man vill ändra buffertens storlek.

> 19. Gör följande
>
> * skapa en pekare till en `struct s { int n; char *p; }`
> * allokera dynamiskt minne för en `struct s { int n; char *p; }` och
> tilldela till pekaren
> * allokera dynamiskt en sträng med ditt namn och tilldela den till
> elementet p i strukturen, använd funktionen `strncpy` och prova att
> använda både punkt- och piloperatorn
> * tilldela ett värde till elementet n i strukturen, prova att
> använda både punkt- och pil-operatorn

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void)
{
    typedef struct {
        int n;
        char *p;
    } s;

    s *sp = malloc(sizeof(s));

    sp->p = malloc(5*sizeof(char));
    strncpy(sp->p, "Axel", 5);
    printf("%s\n", (*sp).p);

    sp->n = 5;
    printf("%d\n", (*sp).n);

    free(sp->p);
    free(sp);

    return 0;
}
```

> 20. Gör följande:
>
> * Skriv en funktion `double **alloc_matrix(int rows, int columns);`
>   som allokerar en tvådimensionell double-array med rows rader och
>   columns kolumner, samt returnerar en pekare till arrayen.
>
> * Skriv en funktion `void sum_and_no_of_neg(double **array, int
>   rows, int columns, double *sum_neg, int *no_of_neg);` som tar emot
>   en pekare, array, till en tvådimensionell array med `rows` rader
>   och `columns` kolumner, i variablen som `sum_neg` pekar på sparar
>   summan av alla negativa tal som finns i array, samt i variablen
>   som `no_of_neg` pekar på sparar antalet negativa tal som finns i
>   array.
>
> * Skriv en funktion `void free_matrix(double **matrix, int rows);`
>   som lämnar tillbaka det minne som allokerats med hjälp av
>   funktionen alloc_matrix.  Parametern matrix är en pekare till
>   matrisen och rows är antalet rader i matrisen.

``` c
double **alloc_matrix(int rows, int columns)
{
    double **m = calloc(rows, sizeof(double*));
    for (int i = 0; i < rows; i++) {
        m[i] = calloc(columns, sizeof(double));
    }

    return m;
}

void sum_and_no_of_neg(double **array, int rows, int columns, double
    *sum_neg, int *no_of_neg)
{
    *sum_neg = 0;
    *no_of_neg = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            *sum_neg += array[i][j] < 0 ? array[i][j] : 0;
            *no_of_neg += array[i][j] < 0 ? 1 : 0;
        }
    }

    printf("Sum of negative values: %g\n", *sum_neg);
    printf("Number of negative values: %d\n", *no_of_neg);
}

void free_matrix(double **matrix, int rows)
{
    for (int i = 0; i < rows; i++) {
        free(matrix[i]);
    }
    free(matrix);
}
```
