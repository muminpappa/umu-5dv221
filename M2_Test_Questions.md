---
title: Imperativ programmering (C) Modul 2
---

# Funktioner och pekare - Testfrågor #

> Du är redo för mästarprovet först när du förstår och kan svara på
> följande frågor.

> 1. Vad är en global variabel och varför bör de användas sparsamt?

En global variabel är en variabel som deklareras utanföra alla
block. Därmed får den en räckvidd över hela programmkoden. Detta gör
att koden blir svårare att underhålla. Dessutom försvårar det att
skriva modulär, återanvändbar kod - den globala variablen "smittar"
moduler genom att tvinga programmeraren att deklarera den som global
även i ny kod.

> 2. Initialisera en pekare p så att den pekar på en variabel a av
> aritmetisk typ. Skriv en sats som ändrar värdet på a via pekaren
> p. Visualisera till sist hur a och p förhåller sig till varandra med
> ”lådor och pilar”.

``` c
int a = 0;
int *p = &a;
*p = 5;
```

> 3. Använd block och deklarationer för att skapa en dinglande pekare.

``` c
int *p;

{
    int a;
    p = &a;
}

/* nu dinglar p */
```

> 4. Skapa en situation där två pekare p och q pekar till samma
> variabel a av aritmetisk typ. Ändra värdet på a via p och observera
> det nya värdet på a genom q. Visualisera till sist hur variablerna
> förhåller sig till varandra med ”lådor och pilar”.

> 5. Givet deklarationen int *p;, beskriv typen av p med ord.

Variablen `p` kan innehålla en pekare dvs en adress i minnet som
innehåller ett objekt av typ heltal.

> 6. Deklarera en funktion foo som tar två heltal som argument och
>    returnerar ett flyttal.

``` c
double foo(int n_1, int n_2);
```

> 7. Deklarera en funktion foo som inte tar några argument men
>    returnerar ett heltal.

``` c
int foo(void);
```

> 8. Deklarera en funktion foo som inte returnerar någonting men har
> tre parametrar av heltalstyp.

``` c
void foo(int n_1, int n_2, int n_3);
```

> 9. Beskriv skillnaden mellan en formell parameter och en aktuellt
>    parameter.

Funktionens deklaration definierar vilka **formella** parametrar som
måste följa med när funktionen anropas. De parametrar som sedan
verkligen används när funktionen anropas kallar man för aktuella
parametrar.

> 10. Anropa följande funktion:
>
> void foo(int x);

``` c
int a = 5;
...
foo(a);
```

> 11. Anropa följande funktion och använd returvärdet i en uttryckssats:
>
> double foo(int a, int b);

``` c
double result = foo(2, 3);
```

> 12. Deklarera funktionen foo så att följande uttryckssats blir
> syntaktiskt korrekt:
>
> a = 5 + foo (3);

``` c
int foo(int number);
```

> 13. Definiera en funktion add som returnerar summan av sina två
>     formella parametrar.

``` c
double add(double n_1, double n_2)
{
    return n_1 + n_2;
}
```

> 14. Definiera en funktion med returtyp void som tar adressen till
> ett heltalsobjekt som argument och ökar värdet på objektet med
> ett. Välj ett bra namn för funktionen.

``` c
void plus_one(int *n)
{
    *n += 1;
}
```

> 15. Definiera en funktion som byter värdet på två
> variabler. Funktion är tänkt att användas så här:
>
> int a = 1, b = 2;
> swap (&a, &b);
> // Nu ska a = 2, b = 1
>
> Välj ett bra namn för funktionen.

``` c
void swap(int *n_1, int *n_2)
{
    int temp = *n_1;
    *n_1 = *n_2;
    *n_2 = temp;
}
```

> 16. Följande funktion påstår sig returnera adressen av sitt
>     argument.
>
> int *address_of(int x)
> {
>
> return &x;
>
> }

> Varför fungerar inte funktionen som det var tänkt, dvs. givet
> deklarationen int a;, varför är värdet på uttrycket &a inte lika med
> värdet på uttrycket address_of(a)?

När man anropar `address_of(a)` så *kopieras* argumentet `a` så att
funktionen kan använda den (den läggs på stacken). Så adressen som
returneras blir den av kopian. Eftersom kopian inte längre finns kvar
när funktionen har återvänt är returvärdet en dinglande pekare.

> 17. Definiera en funktion som kopierar värdet på ett objekt till ett
> annat objekt. Funktionen skall ta adresserna till objekten som
> argument. Välj ett bra namn för funktionen.

``` c
void copy_value(void *src, void *dest)
{
    *dest = *src;
}
```

> 18. Skriv om följande funktion så att den istället för att returnera
> ett värde via en retursats lagrar resultatet i ett objekt vars
> adress skickats till funktionen som en aktuell parameter. Mer
> specifikt, tag funktionen
>
> double area(double width , double height)
> {
>
> return width * height;
>
> }
>
> och skriv om den så att den passar med följande deklaration:
>
> void area(double width , double height , double *area );

``` c
void area(double width , double height , double *area )
{
    *area = width * height;
}
```

> 19. Vad är skillnaden mellan en funktionsdeklaration och en
>     funktionsdefinition?

Deklarationen talar bara om gränssnittet till funktionen, alltså vad
funktionen har för namn, vilka formella parametrar den tar (deras namn
och typ), och vilken typ av resultat den returnerar. Definitionen
däremot talar också om vad funktionen ska göra och hur den ska
åstadkomma det.

> 20. Visualisera med “lådor och pilar” variablerna/objekten a och p
> och deras relation när kontrollflödet är inuti funktionen foo.
>
> void foo(int *p)
> {
>
> *p = 2;
>
> }
>
> int main(void)
> {
>
> int a = 1;
> foo(&a);
>
> }
>
> Använd figuren för att förklara värdet av a efter anropet till foo.

![Relationen mellan a och p när kontrollflödet är inuti funktionen
foo](figures/M2_figure_02.drawio.svg)

Under anropet av `foo()` ändras innehållet på adressen som `p` pekar
på till 2.

> 21. Skriv C-koden för ett komplett, prydligt och strukturerat
> program som (1) läser in två decimaltal, (2) skriver ut de två
> talen, (3) byter plats på de två talen (efteråt ska det tal som var
> i den första variabeln vara i den andra och vice versa), samt (4)
> skriver ut de två talen.
>
> Steg 1 i algoritmen ska ske genom två olika anrop till en funktion
> som läser in ett tal till en variabel av typen double. Det inlästa
> talet ska returneras via funktionens returvärde.
>
> Steg 3 i algoritmen ska ske genom anrop till en funktion swap med
> returtypen void vars två formella parametrar är av typen pekare till
> double. Funktionen ska byta plats på värdena i de objekt som de
> aktuella parametrarna pekar på.
>
> Följande visar hur interaktionen med användaren ska se ut om
> användaren skriver in talen 2 och 4:
>
> Ange tal 1: 2
> Ange tal 2: 4
> Före swap: a = 2.0, b = 4.0
> Efter swap: a = 4.0, b = 2.0


``` c
#include <stdio.h>

double read_double(int number);
void swap(double *n_1, double *n_2);

int main(void)
{

    double a, b;

    /* (1) läser in två decimaltal */
    a = read_double(1);
    b = read_double(2);

    /* (2) skriver ut de två talen, */
    printf("Före swap: a = %.1f, b = %.1f\n", a, b);

    /* (3) byter plats på de två talen (efteråt ska det tal som var i den
    första variabeln vara i den andra och vice versa), samt */
    swap(&a, &b);

    /* (4) skriver ut de två talen. */
    printf("Efter swap: a = %.1f, b = %.1f\n", a, b);

}

double read_double(int number)
{

    double result;

    printf("Ange tal %d: ", number);
    scanf("%lf", &result);

    return result;

}

void swap(double *n_1, double *n_2);
{

    double temp = *n_1;
    *n_1 = *n_2;
    *n_2 = temp;

}
```

> 22. Fortsättning på föregående uppgift. Visualisera med ”lådor och
> pilar” hur de totalt fem variablerna (tre lokala variabler och två
> formella parametrar) relaterar till varandra och vad de har för
> värden precis innan kontrollflödet lämnar funktionen swap. Antag att
> användaren först skrivit in talet 2 och sedan talet 4 (såsom i ut-
> skriften i föregående uppgift).

![Tillståndet innan kontrollflödet lämnar funktionen
`swap()`](figures/M2_figure_03.drawio.svg)


> 23. Betrakta följande C-program:
>
>     #include <stdio.h>
>
>     int foo(int a);
>     int bar(int a);
>
>     int b = 5;
>
>     int main(void)
>     {
>
>     int a = 4;
>     int b = 7;
>     int c, d;
>     int *x, *y;
>
>     x = &a;
>     y = x;
>     *x = b - *y; // a=3
>     y = &b;
>     *y = *y - 3; // b=4
>     printf("a = %d, b = %d\n", a, b);
>
>     c = foo(*x); // c = 3 + 5 = 8
>     d = bar(*y); // d = 4 + 2 = 6
>     printf("c = %d, d = %d\n", c, d);
>
>     return 0;
>
>     }
>
>     int foo(int a)
>     {
>
>     a = a + b;
>
>     return a;
>
>     }
>
>     int bar(int a)
>     {
>
>     return a += 2;
>
>     }

> Vad skriver programmet ut när det körs? Försök lösa uppgiften med
> papper och penna. Skriv ett tecken per "ruta"(mellanrum och
> minustecken räknas som enskilda tecken).



> 24. Följande kod innehåller ett eller flera fel. Ange radnummer för
> den kod som är fel och förklara i text varför den är fel.

``` {.c .numberLines}
#include <stdio.h>

int *foo(void);

int main(void)
{
    int *p = foo ();
    printf("a = %d\n", *p);
    return 0;
}

int *foo(void)
{
    int a = 4;
    return &a;
}
```

På rad 15 returnerar funktionen `foo()` en pekare till den lokala
variablen `a`. Eftersom funktionen avslutas omedelbart därefter lämnas
det allokerade minnet där `a` finns sparat tillbaka till
operativsystemet. Så vi får en dinglande pekare och därmed med hög
sannolikhet ett segmenteringsfel när vi utför programmet.
