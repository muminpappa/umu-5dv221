---
title: Imperativ programmering (C) Modul 4
---

# Iterationssatser #

> Testfrågor
>
> Du är redo för mästarprovet först när du förstår och kan svara på
> följande frågor.
>
> 1. Skriv en funktion som via iteration beräknar summan 1 + 2 + · ·
> · + n där n är en parameter.

``` c
int calc_sum(int n)
{
    int sum = 0;
    for (int i=1; i<=n; i++) {
        sum += i;
    }
    return sum;
}
```

> 2. Skriv en funktion som via iteration beräknar summan $1^2 + 2^2 +
> · · · + n^2$ där n är en parameter.

``` c
int sum_of_squares(int n)
{
    int sum = 0;
    for (int i=1; i<=n; i++) {
        sum += i*i;
    }
    return sum;
}
```


> 3. Skriv en funktion som via iteration skriver ut talsekvensen 1, 2,
> 4, 8, . . . , $2^n$ där n är en parameter.

``` c
void powers_of_two(int n)
{
    int product = 1;
    for (int i=0; i<=n; i++) {
        printf("%d, ", product);
        product *= 2;
    }
}
```

> 4. Skriv en funktion som med hjälp av nästlade loopar skriver ut en
> multiplikationstabell där både antalet rader och antalet kolumner
> är parametrar.

``` c
void multiplication_table(int rows, int columns)
{
    for (int i=1; i<=rows; i++) {
        for (int j=1; j<=columns; j++) {
            printf("%6d", i*j);
        }
    printf("\n");
}
```

> 5. Skriv en funktion som faktoriserar ett positivt heltal n. Exempel: 140 : 2 2 5 7.
> Översätt följande algoritm (rad för rad) till C-kod:
>
> • Skriv ut n följt av ” : ”
>
> • d ← 2
>
> • Så länge som d ≤ n:
>
> – Så länge som n är jämnt delbart med d:
>
> ∗ Skriv ut d följt av ett mellanrum
> ∗ n ← n/d
>
> – d ← d + 1

``` c
void print_factors(int n)
{
    int d;
    printf("%d : ");
    d = 2;
    while (d<=n) {
        while (! (n % d)) {
            printf("%d ", d);
            n /= d;
        }
    d++;
    }
}
```

> 6. Implementera följande funktion, vars syfte är att läsa in ett tal mellan lo och hi.
> Använd en lämplig iterationssats för att fråga användaren igen om valet är utanför
> gränserna.
>
>         int read_int(int lo , int hi);

``` c
int read_int(int lo , int hi)
{
    int rv;

    do {
        printf("Ange ett tal: ");
        scanf("%d", &rv);
    } while ( rv<lo || rv>hi );

    return rv;
}
```

> 7. Konstruera en oändlig loop på tre olika sätt; en per
>    iterationssats.

``` c
for (int i=1; i>0; i++){
    printf("%d\n", i);
}
```

``` c
int n = 10;

while (n > 0){
    printf("%d\n", n);
}
```

``` c
int k = 0;

do {
    k--;
} while (k < 0);
```

> 8. Ett heltal n > 1 är ett primtal om det inte är jämnt delbart med något tal m i
> intervallet 1 < m < n. Definiera följande funktion så att den returnerar true om
> argumentet är ett primtal.
>
>         bool is_prime(int n);

> Använd följande algoritm:
>
> * För varje tal m i intervallet 1 < m < n:
>     * Om n är jämnt delbart med m, returnera false.
> * Returnera true.
>
> Använd en lämplig iterationssats.

``` c
bool is_prime(int n)
{
    for (int m=2; m<n; m++) {
        if (! (n % m))
            return false;
    }
    return true;
}
```

> 9. Använd funktionen `is_prime` från föregående fråga för att skriva
> ut de hundra första primtalen. Använd en lämplig iterationssats.

``` c
int count = 0;
int n = 1;

while (count <= 100){
    if (is_prime(n)){
        printf("%d\n", n);
        count++;
    }
    n++;
}

```

> 10. Definiera en funktion med deklarationen
>
>         void print_figure(int type , int size );
>
> som skriver ut följande fem typer av figurer (vilken typ som ska
> skrivas ut bestäms av parametern type vars värde är mellan 0 och 4).
>
>
> Varje figur är kvadratisk (här 5×5). Storleken på kvadraten bestäms
> av parametern size. Använd iterationssatser kombinerat med
> val-satser. Använd lämpliga satser och sträva efter enkel kod.

``` c
void print_figure(int type , int size )
{
    if (type == 0) {
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++)
                printf("X");
            printf("\n");
        }
    } else if (type == 1) {
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++){
                if (i == 0 || i == size-1 || j == 0 || j == size-1)
                    printf("X");
                else
                    printf(" ");
                }
            printf("\n");
        }
    } else if (type == 2) {
        for (int i=0; i<size; i++){
            for (int j=0; j<=i; j++)
                printf("X");
            printf("\n");
        }
    } else if (type == 3) {
        for (int i=0; i<size; i++){
            for (int j=0; j<i; j++)
                printf(" ");
            for (int j=i; j<size; j++)
                printf("X");
            printf("\n");
        }
    } else if (type == 4) {
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++)
                if (i == 0 || j == 0)
                    printf("X");
            printf("\n");
        }
    }
}
```

> 11. Använd nästlade for-loopar för att skriva ut följande mönster. Inget komplett pro-
> gram eller funktion behövs. Tips: Det är inte nödvändigt att använda if-sats.
>
>         * * * * *
>         * * * * * *
>         * * * * * * *
>         * * * * * * * *
>         * * * * * * * * *

``` c
for (int i=0; i<5; i++) {
    for (int j=0; j<5+i; j++){
        printf("*");
    }
    printf("\n");
}
```

> 12. Skriv om nedanstående kod så den använder while istället för for.
>
> ``` c
> for (int i = 23 ; i < 23 * 23 ; i *= 10) {
>     printf("i = %d\n", i);
> }
> ```

> 13. Under vilka förutsättningar är respektive iterationssats lämplig
>     att använda?

`for`: Om man vet i förväg, hur många iterationer som kommer att
behövas.

`while`: Om det visar sig först under själva loopen när den ska
brytas, och loopen ska köras noll eller fler gånger.

`do ... while`: Om det visar sig först under själva loopen när den ska
brytas, och loopen ska köras minst en gång.

> 14. Formatera om följande kod så den blir prydlig och strukturerad.

``` c
#include <stdio.h>

int main(void)
{
    int s = 3;

    for(int i = 4; i > 0; i -= 3) {
        int j = i;
        while (j < 4) {
            s += i + j;
            j++;
        }
    }
    printf("%d\n", s);

    return 0;
}
```

> 15. Vad skrivs ut av koden i föregående uppgift?

    s = 3
    i = 4
    j = 4
    false
    i = 1
    j = 1
    true
    s = s + i + j = 3 + 1 + 1 = 5

> 16. Definiera en funktion som via iteration skriver ut alla heltal
> mellan talen a och b (inklusive a och b) som är jämnt delbara med 3,
> där a och b är parametrar, samt a > 1 och b > a. I utskriften ska
> talen skiljas åt med ett tomt tecken (mellanslag).

``` c
void multiples_of_three(int a, int b)
{
    int n = a;
    while ( n % 3 )
        n++;
    while (n <= b) {
        printf("%d ", n);
        n += 3;
    }
}
```

> 17. Definiera en funktion som via iteration skriver ut alla heltal
> mellan talen a och b (inklusive a och b) som är udda, där a och b är
> parametrar, samt a > 1 och b > a. I utskriften ska talen skiljas åt
> med ett tomt tecken (mellanslag). Exempel: Om funktionen anropas med
> a = 3 och b = 8, så ska talen 3 5 7 skrivas ut.

``` c
void odd_numbers(int a, int b)
{
    for (int i = a % 2 ? a : a + 1; i <= b; i += 2)
        printf("%d ", i);
    printf("\n");
}
```
