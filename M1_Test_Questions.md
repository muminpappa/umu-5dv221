---
title: Imperativ programmering (C) Modul 1
---

# Variabler och sekvenser av satser #

> Testfrågor
>
> Du är redo för mästarprovet först när du förstår och kan svara på
> följande frågor.

> 1. Ge några olika exempel på heltalstyper och flyttalstyper.

`char`, `int` och `long int` är exempel av heltalstyper, medan `float`
och `double` är flyttalstyper.

> 2. Beskriv några avgörande skillnader mellan flyttalstyper och
>    heltalstyper.

Heltalstyper är exakta, medan flyttalstyper har rundningsfel.

> 3. Skriv ut ett datum på formen YYYY-MM-DD med hjälp av printf. Ange
> datumet med hjälp av tre heltalsvariabler (year, month, day).

``` c++
printf("%04d-%02d-%02d\n", year. month. day);
```

> 4. Lagra din vikt i enheten kg i en flyttalsvariabel vid namn
> weight. Skriv sedan ut vikten med ett hektos precision.

``` c++
double weight = 87.6;
printf("%4.1lf kg\n", weight);
```

> 5. Initialisera en heltalsvariabel count till 23.

``` c++
int count = 23;
```

> 6. Byt ut uttryckssatsen a = (a + 5) * 7 - 3 mot en likvärdig
> sekvens av satser som istället för enkla aritmetiska operatorer
> (t.ex. +) endast använder komposita tilldelningsoperatorer
> (t.ex. +=).

``` c++
a += 5;
a *= 7;
a -= 3;
```

> 7. Vad blir resultatet av följande heltalsdivisioner?
>
> • 8 / 2

4

> • 7 / 2

3

> • -9 / 4

-2

> • 5 / -2

-2

> 8. Vad blir resultatet av följande modulusberäkningar?
>
> • 8 % 2

0

> • 7 % 2

1

> • -9 % 4

-1

> • 5 % -2

1

Obs: Resten får **alltid** täljarens förtecken.

> 9. Antag att n personer skall dela upp sig i g ungefär lika stora
> grupper. Om n är en multipel av g så kan alla grupper bestå av
> precis n/g personer. I annat fall kommer några grupper måsta
> innehålla en extra person.
>
> Räkna ut (a) hur många personer det är i den/de minsta
> gruppen/grupperna och (b) hur många grupper som innehåller en extra
> person. Använd heltalsvariabler och de aritmetiska operatorerna /
> (heltalsdivision) och % (modulo).

``` c++
unsigned int n, g, minsta_gruppstorlek, antal_grupper_med_extra_person;

minsta_gruppstorlek = n/g;
antal_grupper_med_extra_person = n%g;
```

> 10. Om det positiva heltalet a är en multipel av det positiva
> heltalet b, vad blir då värdet av a % b?

0

> Vad kan sägas om värdet av a % b då a inte är en multipel av b?

Då blir `a % b` ett heltal större än 0 men mindre än b.

> 11. Översätt följande matematiska uttryck till ett motsvarande
> uttryck i C. Använd flyttal.
>
> a(b + c)/(d − ef).

``` c++
double a,b,c,d,e,f;
printf("%lf\n", a*(b+c)/(d - e*f));
```

> 12. Översätt följande C-kod till ett motsvarande matematiskt
> uttryck. Antag att variablerna har flyttalstyp.
>
> (a - b) * c + d / (e + f)



> 13. Vad blir resultatet av följande explicita typkonverteringar?
>
> • x = (int) 5.73

5

> • y = (float) (20 / 30)

0.666667

> 14. Låt a och b vara heltalsvariabler. Ange ett uttryck för att
> beräkna deras medelvärde.  Tips: tänk på typer och typkonvertering.

``` c++
(a+b)/2.;
```

> 15. Översätt följande beräkning till en sekvens av satser (en sats
> per mening): “Sätt a till ett godtyckligt heltal mellan 1
> och 9. Sätt b till ett godtyckligt heltal mellan 1
> och 9. Multiplicera a med 5. Addera 7 till a. Dubblera a. Addera a
> till b. Subtrahera 14 från b. Skriv ut b.”

``` c++
a = 3;
b = 8;
a *= 5;
a += 7;
a *= 2;
b += a;
b -= 14;
printf("%d\n", b);
```

> 16. Byt ut följande sekvens av satser mot en likvärdig ensam sats.
>
> int x = a + b;
> x *= c + d;
> x /= 2;

``` c++
x = ((a+b)*(c+d))/2;
```

> 17. För var och en av raderna markerade 1 – 7, ange vilka av
> variablerna som är inom räckvidd där.

``` c++
int a; // (1)
{

    int b; // (2)
    int c; // (3)
    {

        int a; // (4) a2,b1,c
        int b; // (5) a2,b2,c

    }
    // (6) a1,b1,c

}
// (7) a1
```


> 18. Formatera först om (utan att ändra annat än mellanrum och
> radbrytningar) följande kodutdrag (del av ett program) så att den
> blir prydlig och strukturerad (var noga med att tydligt visa
> mellanrum och indentering), ange sedan vad som skrivs ut.


``` c++
int a = 5;
int b = a + 7;

{

    int a = 3;
    int c = a + b;
    printf("%d", a);

}

a = a * b;
```

`printf()` skriver ut 3.

> 19. Formatera först om (utan att ändra annat än mellanrum och
> radbrytningar) följande kod så att den blir prydlig och strukturerad
> (var noga med att tydligt visa mellanrum och indentering), ange
> sedan vad som skrivs ut.

``` c++
#include <stdio.h>

int main(void)
{

    int a = 4;
    int b = 7;
    int c = 10;
    int d;
    {

        int c = b % a;
        c += a / b;
        d = c;

    }
    c *= d;
    printf("%d %d\n", c, d);
    return 0;

}
```

`printf()` skriver ut 30 och 3.

> 20. Byt till mer lämpliga namn på variablerna i följande kod.

``` c++
double length = 5.0, width = 7.3;
double area = length * width;
printf("The area of the rectangle is %.2lf cm^2", area);
```

> 21. Skriv C-koden för ett komplett, prydligt och strukturerat
> program inklusive main- funktionen som läser in tre domarpoäng via
> scanf till heltalsvariabler och sedan skriver ut medelpoängen med
> två decimalers precision. Använd explicit typkonvertering.
> Programmet behöver inte inkludera någon instruktion till användaren.

``` c++
#include <stdio.h>

int main(void)
{

    int p1, p2, p3;

    scanf("%d", &p1);
    scanf("%d", &p2);
    scanf("%d", &p3);

    printf("Medelpoäng: %.2lf\n", ((double)(p1 + p2 + p3)) / 3);

    return 0;

}
```

> 22. Skriv C-koden för ett komplett, prydligt och strukturerat
> program inklusive main- funktionen som läser in en sfärs radie i
> hela centimeter via scanf och sedan skriver ut sfärens area (4 ∗ pi
> ∗ r2) och volym ((4/3) ∗ pi ∗ r3) med en decimals precision.
> Programmet behöver inte inkludera någon instruktion till
> användaren. För pi kan du använda närmevärdet 3,14.

``` c++
#include <stdio.h>

int main(void)
{

    const double pi = 3.14;
    int radius;

    scanf("%d", radius);

    printf("Sfärens area: %.1lf\n",
        4 * pi * radius^2);
    printf("Sfärens volym: %.1lf\n",
        4./3. * pi * radius^3);

    return 0;

}
```
