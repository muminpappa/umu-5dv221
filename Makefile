# https://lincolnmullen.com/blog/a-makefile-to-convert-all-markdown-files-to-pdfs-using-pandoc/

PDFS := $(patsubst %.md,%.pdf,$(wildcard *.md))
HTMLS := $(patsubst %.md,%.html,$(wildcard *.md))

all : $(PDFS) $(HTMLS)

%.pdf : %.md
	pandoc --bibliography=references.bib --csl=ieee.csl \
	-V margin-left=2cm -V margin-right=2cm -V margin-top=2cm -V margin-bottom=2cm \
	--citeproc --pdf-engine=wkhtmltopdf --pdf-engine-opt=--enable-local-file-access \
	--css=pandoc.css --highlight-style=breezedark -f markdown -t html5 -s -o $@ $<

# Remove all PDF outputs
clean :
	rm $(PDFS) $(HTMLS)

# Remove all PDF outputs then build them again
rebuild : clean all

%.html : %.md
	pandoc --bibliography=references.bib --csl=ieee.csl \
	-V margin-left=2cm -V margin-right=2cm -V margin-top=2cm -V margin-bottom=2cm \
	--citeproc --css=pandoc.css -f markdown -t html5 -s -o $@ $<
