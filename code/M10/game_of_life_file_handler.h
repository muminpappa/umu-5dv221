/*
 * Programmering i C
 * VT 2024
 * Mastery test 10
 *
 * File:         game_of_life_file_handler.h
 * Description:  File handlers for Conway's Game of Life.
 * Author:       Axel Franke
 * CS username:  ens23afe
 * Date:         2024-03-10
 */

#ifndef GAME_OF_LIFE_FILE_HANDLER_H
#define GAME_OF_LIFE_FILE_HANDLER_H


#include "game_of_life.h"


/* Declaration of functions */
int load_config_from_file(field *the_field, FILE *fp);
int save_config_to_file(const field the_field, FILE *fp);

#endif
