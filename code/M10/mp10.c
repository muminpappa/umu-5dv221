/*
 * Programmering i C
 * VT 2024
 * Mastery test 10
 *
 * File:         mp10.c
 * Description:  A simple implementation of Conway's Game of Life.
 * Author:       Axel Franke
 * CS username:  ens23afe
 * Date:         2024-03-10
 * Input:        Name of file with initial configuration, and name of file
 *               to store final configuration.
 * Output:       Prints game field to screen in each step.
 *               Saves game field to file when user ends process.
 * Limitations:  Limited validation of input.
 */

#include <stdio.h>
#include <stdlib.h>
#include "game_of_life.h"
#include "game_of_life_file_handler.h"


/* Declaration of functions */
int check_prog_params(int argc, const char *argv[],
                      FILE **in_file_p, FILE **out_file_p);


/* Description: Start and run games, interact with the user.
 * Input:       Two filenames: File with input configuration, and
 *              file to store result.
 * Output:      Prints information to the user, and the game field in
 *              each step. Saves final configuration to output file.
 */

int main(int argc, const char *argv[])
{
    FILE *in_file_p, *out_file_p;
    field gol;
    _Bool proceed;
    int rv = 0;

    rv = check_prog_params(argc, argv, &in_file_p, &out_file_p);

    if (rv == 0) {
        rv = load_config_from_file(&gol, in_file_p);
    }

    if (rv == 0) {
        /* Run the game */
        do {
            print_to_screen(gol);
            proceed = continue_game();
            if (proceed) {
                make_step(&gol);
            }
        } while ( proceed );

        save_config_to_file(gol, out_file_p);
        destroy_field(&gol);
    }

	return rv;
}


/* Description: Checks the parameters to the program. Checks if the
*               call to the program has the right number of
*               parameters. Open the input and output files.
* Input:        The parameters to the program and two pointers to file
*               pointers.
* Output:       Returns 0 when the files are correctly opened.
*               Returns a non-zero value on failure.
*/

int check_prog_params(int argc, const char *argv[],
                      FILE **in_file_p, FILE **out_file_p)
{
    if (argc != 3) {
        fprintf(stderr, "Usage: %s ", argv[0]);
        fprintf(stderr,
                "<input configuration file> <output configuration file>\n");
        return 1;
    }

    *in_file_p = fopen(argv[1], "r");
    if (*in_file_p == NULL) {
        fprintf(stderr, "Could not open the file: %s\n", argv[1]);
        return 1;
    }

    *out_file_p = fopen(argv[2], "w");
    if (*out_file_p == NULL) {
        fprintf(stderr, "Could not open the file: %s\n", argv[2]);
        fclose(*in_file_p);
        return 1;
    }

    return 0;
}
