/*
 * Programmering i C
 * VT 2024
 * Mastery test 10
 *
 * File:         game_of_life.h
 * Description:  Functions for Conway's Game of Life.
 * Author:       Axel Franke
 * CS username:  ens23afe
 * Date:         2024-03-10
 */


#ifndef GAME_OF_LIFE_H
#define GAME_OF_LIFE_H


/* Constants, representation of states */
#define ALIVE 'X'
#define DEAD '.'

#include <stdio.h>


/* Declaration of data structure */
typedef struct {
    char current;
    char next;
} cell;

typedef struct {
    int rows;
    int cols;
    cell **cells;
} field;


/* Declaration of functions */
void print_to_screen(const field f);
void make_step(field *f);
void calc_next(field *f);
void update_current(field *f);
int count_neighbors(const field f, int pos_y, int pos_x);
_Bool continue_game(void);
void allocate_cells(field *f);
void destroy_field(field *f);

#endif
