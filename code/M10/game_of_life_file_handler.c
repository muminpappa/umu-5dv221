/*
 * Programmering i C
 * VT 2024
 * Mastery test 10
 *
 * File:         game_of_life_file_handler.c
 * Description:  File handlers for Conway's Game of Life.
 * Author:       Axel Franke
 * CS username:  ens23afe
 * Date:         2024-03-10
 */

#include "game_of_life_file_handler.h"


/* Description: Loads a configuration to the field structure from a
*               file. It is the responsiblity of the caller to
*               deallocate the dynamically allocated memory in the field
*               structure through a call to the function destroy_field.
*               The file pointed to by fp is closed.
* Input:        A pointer to where the created field structure should be
*               assigned and a file pointer to the file with the initial
*               configuration.
* Output:       Returns 0 on success, the field structure is created
*               with the configuration from the file.
*               Returns a non-zero value on failure.
*/

int load_config_from_file(field *the_field, FILE *fp)
{
    if (2 != fscanf(fp, "%d,%d\n", &the_field->rows, &the_field->cols)) {
        fprintf(stderr, "Incorrect configuration file format\n");
        return 1;
    }

    allocate_cells(the_field);

    for (int i = 0; i < the_field->rows; i++) {
        for (int j = 0; j < the_field->cols; j++) {
            char c = (char) fgetc(fp);
            the_field->cells[i][j].current = c=='*' ? ALIVE : DEAD;
        }
        // skip newline
        fgetc(fp);
    }

    fclose(fp);

    return 0;
}


/* Description: Saves the current configuration of the field to a
*               specified file. The file pointed to by fp is closed.
* Input:        The field structure and a file pointer to the file
*               where the final configuration should be saved.
* Output:       Returns 0 on success, the current configuration in the
*               field is written to the file.
*               Returns a non-zero value on failure.
*/

int save_config_to_file(const field the_field, FILE *fp)
{
    fprintf(fp, "%d,%d\n", the_field.rows, the_field.cols);
    for (int i = 0; i < the_field.rows; i++) {
        for (int j = 0; j < the_field.cols; j++) {
            char c = the_field.cells[i][j].current == ALIVE ? '*' : '0';
            fprintf(fp, "%c", c);
        }
        // write newline
        fprintf(fp, "\n");
    }
    fclose(fp);

    return 0;
}
