# Frågor om MP10 #

* Ska man verkligen ha kvar `init_field()` i MP10?
* Funktionsargument: `function(field f)` vs `function(const field *f)`
  vs `function(const field f)` - delar av en `struct` (t ex det som en
  pekare pekar på) ska inte heller ändras?
* Villkorsoperatorn `a ? b : c` leder till anmärkning? Varför?
* Bör även .h filer innehålla en beskrivning i toppen?
