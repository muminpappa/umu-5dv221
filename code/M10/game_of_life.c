/*
 * Programmering i C
 * VT 2024
 * Mastery test 10
 *
 * File:         game_of_life.c
 * Description:  Functions for Conway's Game of Life.
 * Author:       Axel Franke
 * CS username:  ens23afe
 * Date:         2024-03-10
 */


#include <stdlib.h>
#include <stdio.h>
#include "game_of_life.h"
#include "game_of_life_file_handler.h"


/* Description: Prints the field to the screen.
 * Input:       The field structure
 * Output:      None
 */

void print_to_screen(const field f)
{
    for (int r = 0; r < f.rows; r++) {
        for (int c = 0; c < f.cols; c++) {
            printf("%c ", f.cells[r][c].current);
        }
        printf("\n");
    }
}


/* Description: Check if user wants to continue the game
 * Input:       None
 * Output:      True means YES, anything else means NO
 */

_Bool continue_game(void)
{
    printf("Select one of the following options:\n");
	printf("\t(enter)\tStep\n");
	printf("\t(any)\tSave and exit\n");

    return getchar() == '\n';
}


/* Description: Allocate the memory for the cells array
 * Input:       The field structure
 * Output:      The field structure with a cells array allocated
 */

void allocate_cells(field *f)
{
    f->cells = calloc(f->rows, sizeof(cell*));
    for (int i = 0; i < f->rows; i++) {
        f->cells[i] = calloc(f->cols, sizeof(cell));
    }
}


/* Description: Deallocate the memory in the cells array
 * Input:       The field structure
 * Output:      The field structure with an empty cells array
 */

void destroy_field(field *f)
{
    for (int i = 0; i < f->rows; i++) {
        free(f->cells[i]);
    }
    free(f->cells);
}


/* Description: Execute one step of Game of Life
 * Input:       The field structure
 * Output:      The updated field structure
 */

void make_step(field *f)
{
    calc_next(f);
    update_current(f);
}


/* Description: Calculate the next status of the game
 * Input:       The field structure
 * Output:      The cells array in the field structure is updated
 */

void calc_next(field *f)
{
    for (int r = 0; r < f->rows; r++) {
        for (int c = 0; c < f->cols; c++) {
            int n = count_neighbors(*f, r, c);
            switch (n) {
            case 0:
            case 1:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                f->cells[r][c].next = DEAD;
                break;
            case 2:
                f->cells[r][c].next = f->cells[r][c].current;
                break;
            case 3:
                f->cells[r][c].next = ALIVE;
                break;
            }
        }
    }
}


/* Description: Update the current element in each position
 * Input:       The field structure
 * Output:      The "current" element of the cells array is updated
 */

void update_current(field *f)
{
    for (int r = 0; r < f->rows; r++) {
        for (int c = 0; c < f->cols; c++) {
            f->cells[r][c].current = f->cells[r][c].next;
        }
    }
}


/* Description: Count the neighbors that are alive
 * Input:       The field structure and a position in it
 * Output:      The number of neighbors fo rthis position
 */

int count_neighbors(const field f, int row, int col)
{
    int n = 0;

    for (int r = row - 1; r <= row + 1; r++) {
        for (int c = col - 1; c <= col + 1; c++) {
            _Bool is_neighbor =  (c != col) || (r != row);
            is_neighbor = is_neighbor && (c >= 0) && (c < f.cols);
            is_neighbor = is_neighbor && (r >= 0) && (r < f.rows);
            if (is_neighbor && f.cells[r][c].current == ALIVE) {
                n++;
            }
        }
    }

    return n;
}
