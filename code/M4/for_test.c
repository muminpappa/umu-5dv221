#include <stdio.h>

int main(void)
{
    int n = 0;
    for(int i=0; i<10; n += i++);
    printf("n = %d\n", n);
    return 0;
}
