#include <stdio.h>

int main(void)
{

   int n1 = 1;
   int n2 = -2;
   int n3 = 1;

   for (int i = 0 ; i <= n3 ; i++) { // i = 0, 1
       for (int j = i ; j > n2 ; j--) { // j = 0, -1 and j = 1, 0, -1
         n1 *= 2;
      }
   }
   // n1 = 2*2*2*2*2 = 32

   printf("n1 = %d\n", n1);


   return 0;
}
