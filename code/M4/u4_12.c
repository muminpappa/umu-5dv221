#include <stdio.h>

int main(void)
{
    int end;

    printf("Hur många varv? ");
    scanf("%d", &end);

    printf("\n i \t i*i \t i*i*i \n");
    printf("===\t=====\t=======\n");
    for (int i=1; i<=end; i++) {
        int out = i;
        for (int k=1; k<=3; k++){
            printf("%2d\t", out);
            out *= i;
        }
        printf("\n");
    }
    return 0;
}
