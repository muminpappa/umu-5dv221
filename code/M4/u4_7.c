#include <stdio.h>

int main(void)
{
    int sum = 0, n;
    printf("End: ");
    scanf("%d", &n);
    for (int i = 1 ; i <= n ; i++) {
        sum += i;
        printf("%4d : %4d\n", i, sum);
    }
    return 0;
}
