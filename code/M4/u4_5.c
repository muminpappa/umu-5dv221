#include <stdio.h>

int main(void)
{
    int start, end;
    printf("Start: ");
    scanf("%d", &start);
    printf("End: ");
    scanf("%d", &end);
    for (int i = start ; i <= end ; i++) {
        printf("%d ", i);
    }
    return 0;
}
