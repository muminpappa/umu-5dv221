#include <stdio.h>

void info(void);
int choose(void);
void check_choice(int choice);
double enter_temperature(int choice);
double convert_temperature(int choice, double temperature);
void print_temperature(int choice, double temperature, double new_temperature);

int main(void)
{
    int choice;
    double temperature;
    double new_temperature;

    do {

        info();
        choice = choose();
        check_choice(choice);

    } while (choice < 1 || choice > 2);

    temperature = enter_temperature(choice);

    new_temperature = convert_temperature(choice, temperature);

    print_temperature(choice, temperature, new_temperature);

    return 0;
}

void info(void)
{
    printf("1. Omvandla från F till C\n");
    printf("2. Omvandla från C till f\n");
}

int choose(void)
{
    int choice;

    printf("Ditt val: ");
    scanf("%d", &choice);
    return choice;
}

void check_choice(int choice)
{
    if (choice < 1 || choice > 2) {
        printf("Felaktigt val\n");
    }
}

double enter_temperature(int choice)
{
    double temperature;

    if (choice == 1) {
        printf("Temperatur i Fahrenheit: ");
    }
    else {
        printf("Temperatur i Celsius: ");
    }
    scanf("%lf", &temperature);
    return temperature;
}

double convert_temperature(int choice, double temperature)
{
    double new_temperature;

    if (choice == 1) {
        new_temperature = (temperature - 32) / (9.0/5.0);
    }
    else {
        new_temperature = (9.0/5.0) * temperature + 32;
    }
    return new_temperature;
}

void print_temperature(int choice, double temperature, double new_temperature)
{
    if (choice == 1) {
        printf("%.2f F är %.2f C\n", temperature, new_temperature);
    }
    else {
        printf("%.2f C är %.2f F\n", temperature, new_temperature);
    }
}
