#include <stdio.h>

int sum(int n);

int main (void)
{

    for (int i=1; i<=6; i++){
        printf("sum(%d) = %d\n", i, sum(i));
    }

    return 0;
}

int sum(int n)
{
    int rv = 0;

    if (n > 0){
        rv = n + sum(n-1);
    }
    return rv;
}
