#include <stdio.h>

int main(void)
{
    int sum = 0, n;
    for (int i = 1 ; i <= 10 ; i++) {
        sum += i;
        printf("%4d : %4d\n", i, sum);
    }
    return 0;
}
