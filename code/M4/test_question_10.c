#include <stdio.h>

void print_figure(int type , int size );

int main(void){
    for (int i=0; i<5; i++){
        print_figure(i, 5);
        printf("\n\n");
    }
    return 0;
}

void print_figure(int type , int size )
{
    if (type == 0) {
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++)
                printf("X");
            printf("\n");
        }
    } else if (type == 1) {
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++){
                if (i == 0 || i == size-1 || j == 0 || j == size-1)
                    printf("X");
                else
                    printf(" ");
                }
            printf("\n");
        }
    } else if (type == 2) {
        for (int i=0; i<size; i++){
            for (int j=0; j<=i; j++)
                printf("X");
            printf("\n");
        }
    } else if (type == 3) {
        for (int i=0; i<size; i++){
            for (int j=0; j<i; j++)
                printf(" ");
            for (int j=i; j<size; j++)
                printf("X");
            printf("\n");
        }
    } else if (type == 4) {
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++)
                if (i == 0 || j == 0)
                    printf("X");
            printf("\n");
        }
    }
}
