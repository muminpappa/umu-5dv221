#include <stdio.h>

void odd_numbers(int a, int b);

int main(void){
    odd_numbers(1, 2);
    odd_numbers(4, 13);
    odd_numbers(-4, 9);
    return 0;
}

void odd_numbers(int a, int b)
{
    for (int i = a % 2 ? a : a + 1; i <= b; i += 2)
        printf("%d ", i);
    printf("\n");
}
