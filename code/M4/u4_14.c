#include <stdio.h>

int main(void)
{

   int x1 = 8;
   int x2 = 3;
   int x3 = x1 % x2;   // 2



   while (x1 >= x2) {  // 8 > 3
       x1--; // 7 6 5 4 3 2
       x3 += x1;  // 9 15 20 24 27 29
   }

   // x1 = 2
   // x2 = 3
   // x3 = 29

   printf("x1 = %d, x2 = %d, x3 = %d\n", x1, x2, x3);

   return 0;
}
