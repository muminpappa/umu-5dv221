#include <stdio.h>

void function_a(int start, int end);
void function_b(int start, int end);
void function_c(int start, int end);
void function_d(int start, int end);

int main(void)
{
    int start, end;
    char which;
    
    printf("Start: ");
    scanf("%d", &start);
    printf("End: ");
    scanf("%d", &end);

    printf("Choose [abcd]: ");
    scanf(" %c", &which);

    switch(which){
    case 'a': function_a(start, end);break;
    case 'b': function_b(start, end);break;
    case 'c': function_c(start, end);break;
    case 'd': function_d(start, end);break;
    default: return 1;
    }
    
    return 0;
}

void function_a(int start, int end)
{
    for (int i=start; i<=end; i++) {
        printf("%d ", i);
    }
    printf("\n");
}

void function_b(int start, int end)
{
    for (int i=start; i<=end; i++) {
        printf("%d ", 2*i-1);
    }
    printf("\n");
}

void function_c(int start, int end)
{
    for (int i=end; i>=start; i -= 2) {
        printf("%d ", i);
    }
    printf("\n");
}

void function_d(int start, int end)
{
    for (int i=start; i<=end; i++) {
        printf("%.1f ", i*0.5);
    }
    printf("\n");
}
