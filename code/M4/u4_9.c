#include <stdio.h>

int main(void)
{
    int i = 0;
    int end;

    printf("End: ");
    scanf("%d", &end);

    while (i < end) {
        printf("%d ", i + 1);
        i++;
    }

    return 0;
}
