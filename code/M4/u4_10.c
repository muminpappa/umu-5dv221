#include <stdio.h>

int main(void)
{
    int i = 0;
    int end;
    float in, sum = 0;

    printf("Hur många varv? ");
    scanf("%d", &end);

    while (i < end) {
        printf("Ange ett flyttal: ");
        scanf("%f", &in);
        sum += in;
        i++;
    }
    printf("Summan är %.2f\n", sum);
    return 0;
}
