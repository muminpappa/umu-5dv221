#include <stdio.h>

void multiples_of_three(int a, int b);

int main(void){
    multiples_of_three(1, 2);
    multiples_of_three(4, 13);
    multiples_of_three(-4, 9);
    return 0;
}

void multiples_of_three(int a, int b)
{
    int n = a;
    while ( n % 3 )
        n++;
    while (n <= b) {
        printf("%d ", n);
        n += 3;
    }
    printf("\n");
}
