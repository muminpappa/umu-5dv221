#include <stdio.h>

int main(void)
{
    int n = 0;
    for (int i = 1 ; i <= 10 ; i++) {
        n += i;
        printf("%d : %d", i, n);
    }
    return 0;
}
