/*
 * Programmering i C
 * Spring 24
 * Mastery test 5
 *
 * File:         mp5.c
 * Description:  A simple shopping assistant
 * Author:       Axel Franke
 * CS username:  ens23afe
 * Date:         2024-02-07
 * Input:        Exchange rate and prices to convert
 * Output:       Sum in foreign currency and in SEK
 * Limitations:  No validation of input.
 */

#include <stdio.h>

/* Declaration of functions */
void print_menu(float exchange_rate);
int get_choice(void);
float get_exchange_rate(void);
void sum_prices(float *sum);
void print_sums(float rate, float sum);
float get_price(void);

/* Description: Start and run the shopping assistant, interact with the user.
 * Input:       Exchange rate and prices in foreign currency
 * Output:      Totals in foreign currency and SEK
 */

int main(void)
{
    int choice;
    float exchange_rate = 1.0, sum;

    printf("\nYour shopping assistant\n");

    do {
        sum = 0;
        print_menu(exchange_rate);
        choice = get_choice();

        switch (choice) {
        case 1:
            exchange_rate = get_exchange_rate();
            break;
        case 2:
            sum_prices(&sum);
            print_sums(exchange_rate, sum);
            break;
        case 3:
            printf("\nEnd of program!\n\n");
            break;
        default:
            printf("\nNot a valid choice!\n");
            break;
        }
    } while (choice != 3);

    return 0;
}

/* Description: Present a menu to the user
 * Input:       None
 * Output:      None
 */

void print_menu(float rate)
{
    printf("\n");
    printf("1. Set exchange rate in SEK (current rate: %.2f)\n",
           rate);
    printf("2. Convert prices from the foreign currency\n");
    printf("3. End\n\n");
}

/* Description: Get the user's choice
 * Input:       None
 * Output:      Number of chosen option
 */

int get_choice(void)
{
    int choice = 0;

    printf("Give your choice (1 - 3): ");
    scanf("%d", &choice);

    return choice;
}

/* Description: Read in exchange rate from user
 * Input:       None
 * Output:      Exchange rate in SEK
 */

float get_exchange_rate(void)
{
    float rate;

    printf("\nGive exchange rate: ");
    scanf("%f", &rate);

    return rate;
}

/* Description: Gets list of prices and prints the totals in
 * original currency and in SEK
 * Input:       Exchange rate
 * Output:      None
 */

void sum_prices(float *sum)
{
    float price;

    *sum = 0;
    printf("\n");

    do {
        price = get_price();
        if (price >= 0) {
            *sum += price;
        }
    } while (price >= 0);

}

/* Description: Print sum in foreign currency and in SEK
 * Input:       Exchange rate and sum in foreign currency
 * Output:      None
 */

void print_sums(float rate, float sum)
{
    printf("\n");
    printf("Sum in foreign currency: %.2f\n", sum);
    printf("Sum in SEK: %.2f\n", sum * rate);
}

/* Description: Prompts user for price and gets input
 * Input:       None
 * Output:      Price as a floating point number
 */

float get_price(void)
{
    float price;

    printf("Give price (finish with < 0): ");
    scanf("%f", &price);

    return price;
}
