#include <stdio.h>

int main(void)
{
    double sausage_weight;
    double kilos_per_sausage = 0.1;
    double no_guests;

    printf("Hur mycket korv har du? ");
    scanf("%lf", &sausage_weight);

    no_guests = (sausage_weight -5*kilos_per_sausage)/(2*kilos_per_sausage);


    printf("Antal gäster du kan bjuda: %lf\n", no_guests);

    return 0;
}
