#include <stdio.h>

int main(void)
{
    float number_1, number_2=0;
    
    printf("Tal 1: ");
    scanf("%f", &number_1);

    printf("Tal 2: ");
    scanf("%f", &number_2);


    printf("Summan är: %5.2f\n", number_1 + number_2);
    printf("Differensen är: %5.2f\n", number_1 - number_2);
    printf("Produkten är: %5.2f\n", number_1 * number_2);
    printf("Kvotienten är: %5.2f\n", number_1 / number_2);

    return 0;
}
