#include <stdio.h>

int main(void)
{
    int no_sausages, no_guests=0;
    printf("Enter the number of guests: ");
    scanf("%d", &no_guests);
    no_sausages = 2*no_guests + 5;
	printf("For %d guests you need %d sausages\n",
           no_guests, no_sausages);
    return 0;
}
