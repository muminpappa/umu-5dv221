#include <stdio.h>

int main(void)
{
    float mileage_old, mileage_new, fuel_consumption=0;
    
    printf("Dagens mätarställning (mil): ");
    scanf("%f", &mileage_new);

    printf("Mätarställning för ett år sedan (mil): ");
    scanf("%f", &mileage_old);

    printf("Förbrukat drivmedel (liter): ");
    scanf("%f", &fuel_consumption);

    printf("Drivmedelsförbrukning i liter/mil: %f\n",
           fuel_consumption/(mileage_new - mileage_old));

    return 0;
}
