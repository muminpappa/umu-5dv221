#include <stdio.h>

int main(void)
{
    int number_1, number_2=0;
    
    printf("Tal 1: ");
    scanf("%d", &number_1);

    printf("Tal 2: ");
    scanf("%d", &number_2);


    printf("Summan är: %d\n", number_1 + number_2);
    printf("Differensen är: %d\n", number_1 - number_2);
    printf("Produkten är: %d\n", number_1 * number_2);
    printf("Kvotienten är: %d\n", number_1 / number_2);

    return 0;
}
