#include <stdio.h>

int add(int n1, int n2);

int main(void)
{
    int number_1 = 123;
    int number_2 = 456;
    int result;

    result = add(number_1, number_2);

    printf("Summan är %d\n", result);

    return 0;
}

int add(int n1, int n2)
{
    return n1 + n2;
}
