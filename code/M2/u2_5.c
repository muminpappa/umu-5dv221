#include <stdio.h>

int read_number();

int main(void)
{
    int number_1, number_2=0;

    number_1 = read_number();
    number_2 = read_number();

    printf("Summan är: %d\n", number_1 + number_2);
    printf("Differensen är: %d\n", number_1 - number_2);
    printf("Produkten är: %d\n", number_1 * number_2);
    printf("Kvotienten är: %d\n", number_1 / number_2);

    return 0;
}

int read_number()
{

    int number;

    printf("Mata in ett tal: ");
    scanf("%d", &number);

    return number;

}
