#include <stdio.h>

int foo(int number);

int main(void)
{
    int a = 5;
    printf("%d\n", foo(a));
    return 0;
}

int foo(int number)
{
    return number += 2;
}
