#include <stdio.h>

void print_text(void);
double sum(double n_1, double n_2);
double read_number(void);

int main(void)
{
    double number_1, number_2, number_3;
    double sum_2, sum_3;

    print_text();

    number_1 = read_number();
    number_2 = read_number();
    number_3 = read_number();

    sum_2 = sum(number_1, number_2);
    sum_3 = sum(sum_2, number_3);

    printf("%f + %f = %f\n", number_1, number_2, sum_2);
    printf("%f + %f + %f = %f\n", number_1, number_2, number_3, sum_3);

    return 0;
}


void print_text(void)
{
    printf("\nProgrammet läser in tre tal. ");
    printf("Beräknar summan av de två första talen, \n");
    printf("summan av de tre talen, samt skriver ut ");
    printf("resultaten.\n\n");
}


double read_number(void)
{
    double number;

    printf("Skriv in tal: ");
    scanf("%lf", &number);

    return number;
}



double sum(double n_1, double n_2)
{
    return n_1 + n_2;
}
