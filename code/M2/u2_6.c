#include <stdio.h>

double pyramide_volume(double area, double height);

int main(void)
{

    const double area = 5, height = 3;
    double volume = pyramide_volume(area, height);

    printf("Pyramidens volym: %.1f\n", volume);

    return 0;

}

double pyramide_volume(double area, double height)
{
    return area * height / 3;
}
