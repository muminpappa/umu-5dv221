#include <stdio.h>

int read_number();

int main(void)
{
    int number;

    number = read_number();

    printf("Du skrev in talet %d\n", number);

    return 0;
}

int read_number()
{

    int number;

    printf("Mata in ett tal: ");
    scanf("%d", &number);

    return number;

}
