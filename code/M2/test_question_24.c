#include <stdio.h>

int *foo(void);

int main(void)
{
    int *p = foo ();
    printf("a = %d\n", *p);
    return 0;
}

int *foo(void)
{
    int a = 4;
    return &a; // this should give a segfault
}
