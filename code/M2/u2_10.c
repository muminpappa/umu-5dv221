#include <stdio.h>

void swap_int(int *n_1, int *n_2);

int main(void)
{

    int number_1 = 1, number_2 = 2;

    printf("number_1 = %d, number_2 = %d\n",
           number_1, number_2);
    swap_int(&number_1, &number_2);
    printf("number_1 = %d, number_2 = %d\n",
           number_1, number_2);

    return 0;

}

void swap_int(int *n_1, int *n_2)
{

    int temp = *n_1;
    *n_1 = *n_2;
    *n_2 = temp;

}
