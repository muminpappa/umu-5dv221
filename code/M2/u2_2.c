#include <stdio.h>

double f_add(double n1, double n2);

int main(void)
{
    int number_1 = 123;
    int number_2 = 456;
    int result;

    result = f_add(number_1, number_2);

    printf("Summan är %f\n", result);

    return 0;
}

double f_add(double n1, double n2)
{
    return n1 + n2;
}
