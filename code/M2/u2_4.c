#include <stdio.h>

float f_read_number(void);

int main(void)
{
    float number_1, number_2;

    number_1 = f_read_number();
    number_2 = f_read_number();

    printf("Summan är: %5.2f\n", number_1 + number_2);
    printf("Skillnaden är: %5.2f\n", number_1 - number_2);
    printf("Produkten är: %5.2f\n", number_1 * number_2);
    printf("Kvoten är: %5.2f\n", number_1 / number_2);

    return 0;
}

float f_read_number(void)
{

    float number;
    printf("Mata in ett tal: ");
    scanf("%f", &number);
    return number;
    
}
