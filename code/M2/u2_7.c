#include <stdio.h>

float average_3(int number_1, int number_2, int number_3);

int main(void)
{
    int number_1 = 5, number_2 = 15, number_3 = 7;
    float avg = average_3(number_1, number_2, number_3);

    printf("Average: %.1f\n", avg);

    return 0;

}

float average_3(int number_1, int number_2, int number_3)
{

    return number_1 / 3. + number_2 / 3. + number_3 / 3.;

}
