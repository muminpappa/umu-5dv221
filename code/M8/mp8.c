/*
 * Programmering i C
 * VT 2024
 * Mastery test 8
 *
 * File:         mp8.c
 * Description:  A simple implementation of Conway's Game of Life.
 * Author:       Axel Franke
 * CS username:  ens23afe
 * Date:         2024-02-28
 * Input:        Choice of initial configuration and then instruction
 *               to step or exit.
 * Output:       Prints the game field in each step.
 * Limitations:  No validation of input.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Constants, representation of states */
#define ALIVE 'X'
#define DEAD '.'
#define COLS 20
#define ROWS 20

/* Declaration of data structure */
typedef struct {
	char current;
	char next;
} cell;

/* Declaration of functions */
void init_field(int rows, int cols, cell field[rows][cols]);
void load_glider(int rows, int cols, cell field[rows][cols]);
void load_semaphore(int rows, int cols, cell field[rows][cols]);
void load_random(int rows, int cols, cell field[rows][cols]);
void load_custom(int rows, int cols, cell field[rows][cols]);
void print_to_screen(int rows, int cols, const cell field[rows][cols]);
void make_step(int rows, int cols, cell field[rows][cols]);
void calc_next(int rows, int cols, cell field[rows][cols]);
void update_current(int rows, int cols, cell field[rows][cols]);
int count_neighbors(int rows, int cols, const cell field[rows][cols],
                    int pos_y, int pos_x);
_Bool continue_game(void);

/* Description: Start and run games, interact with the user.
 * Input:       About what initial structure and whether to step or
 *              exit.
 * Output:      Prints information to the user, and the game field in
 *              each step.
 */

int main(void)
{
    cell world[ROWS][COLS];

    init_field(ROWS, COLS, world);

    /* Run the game */
    do {
        print_to_screen(ROWS, COLS, world);
        make_step(ROWS, COLS, world);
    } while ( continue_game() );

	return 0;
}


/* Description: Initialize all the cells to dead, then asks the user
 *              about which structure to load, and finally load the
 *              structure.
 * Input:       The field array and its size.
 * Output:      The field array is updated.
 */

void init_field(int rows, int cols, cell field[rows][cols])
{
	for (int r = 0 ; r < rows ; r++) {
		for (int c = 0 ; c < cols ; c++) {
			field[r][c].current = DEAD;
		}
	}

	printf("Select field spec to load ([G]lider, [S]emaphore, ");
	printf("[R]andom or [C]ustom): ");

	int ch = getchar();

	/* Ignore following newline */
	if (ch != '\n') {
		getchar();
	}

	switch (ch) {
		case 'g':
		case 'G':
			load_glider(rows, cols, field);
			break;
		case 's':
		case 'S':
			load_semaphore(rows, cols, field);
			break;
		case 'r':
		case 'R':
			load_random(rows, cols, field);
			break;
		case 'c':
		case 'C':
		default:
			load_custom(rows, cols, field);
	}
}


/* Description: Inserts a glider into the field.
 * Input:       The field array and its size.
 * Output:      The field array is updated.
 */

void load_glider(int rows, int cols, cell field[rows][cols])
{
	field[0][1].current = ALIVE;
	field[1][2].current = ALIVE;
	field[2][0].current = ALIVE;
	field[2][1].current = ALIVE;
	field[2][2].current = ALIVE;
}


/* Description: Inserts a semaphore into the field.
 * Input:       The field array and its size.
 * Output:      The field array is updated.
 */

void load_semaphore(int rows, int cols, cell field[rows][cols])
{
	field[8][1].current = ALIVE;
	field[8][2].current = ALIVE;
	field[8][3].current = ALIVE;
}


/* Description: Inserts a random structure into the field.
 * Input:       The field array and its size.
 * Output:      The field array is updated. There is a 50 % chance
 *              that a cell is alive.
 */

void load_random(int rows, int cols, cell field[rows][cols])
{
    srand(time(NULL));
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            if (rand() % 2) {
                field[r][c].current = ALIVE;
            }
        }
    }

}


/* Description: Lets the user specify a structure that then
 *              is inserted into the field.
 * Input:       The field array and its size.
 * Output:      The field array is updated.
 */

void load_custom(int rows, int cols, cell field[rows][cols])
{
	printf("Give custom format string: ");
	do {
		int r, c;
		scanf("%d,%d", &r, &c);
		field[r][c].current = ALIVE;
	} while (getchar() != '\n');
}


/* Description: Prints the field to the screen.
 * Input:       The field array and its size.
 * Output:      None
 */

void print_to_screen(int rows, int cols, const cell field[rows][cols])
{
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            printf("%c ", field[r][c].current);
        }
        printf("\n");
    }
}


/* Description: Execute one step of Game of Life
 * Input:       The field array and its size.
 * Output:      The field array is updated
 */

void make_step(int rows, int cols, cell field[rows][cols])
{
    calc_next(rows, cols, field);
    update_current(rows, cols, field);
}


/* Description: Calculate the next status of the game
 * Input:       The field array and its size.
 * Output:      The field array is updated
 */

void calc_next(int rows, int cols, cell field[rows][cols])
{
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            int n = count_neighbors(ROWS, COLS, field, r, c);
            switch (n) {
            case 0:
            case 1:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                field[r][c].next = DEAD;
                break;
            case 2:
                field[r][c].next = field[r][c].current;
                break;
            case 3:
                field[r][c].next = ALIVE;
                break;
            }
        }
    }
}


/* Description: Update the current element in each position
 * Input:       The field array and its size.
 * Output:      The the "current" element of the field array is updated
 */

void update_current(int rows, int cols, cell field[rows][cols])
{
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            field[r][c].current = field[r][c].next;
        }
    }
}

/* Description: Count the neighbors that are alive
 * Input:       The field array and its size.
 * Output:      The number of neighbors
 */

int count_neighbors(int rows, int cols, const cell field[rows][cols],
                    int row, int col)
{
    int n = 0;

    for (int r = row - 1; r <= row + 1; r++) {
        for (int c = col - 1; c <= col + 1; c++) {
            _Bool is_neighbor =  (c != col) || (r != row);
            is_neighbor = is_neighbor && (c >= 0) && (c < COLS);
            is_neighbor = is_neighbor && (r >= 0) && (r < ROWS);
            if (is_neighbor && field[r][c].current == ALIVE) {
                n++;
            }
        }
    }

    return n;
}

/* Description: Check if user wants to continue the game
 * Input:       None
 * Output:      True means YES, anything else means NO
 */

_Bool continue_game(void)
{
    printf("Select one of the following options:\n");
	printf("\t(enter)\tStep\n");
	printf("\t(any)\tExit\n");

    return getchar() == '\n';
}
