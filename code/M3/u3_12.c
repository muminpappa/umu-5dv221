#include <stdio.h>

int main(void)
{
    int year;

    printf("Skriv in ett årtal > ");
    scanf("%d", &year);

    if (year % 4 || (!(year % 100) && (year % 400))) {
        printf("%d är inget skottår\n", year);
    } else
        printf("%d är ett skottår\n", year);
    
    return 0;

}
