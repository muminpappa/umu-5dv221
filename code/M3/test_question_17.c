#include <stdio.h>

void even_or_odd(int n);
int read_number(const int n);

int main (void)
{
    int n = read_number(1);
    even_or_odd(n);
    return 0;
}

int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}

void even_or_odd(int n)
{

    if (n == 1)
        printf("Udd\n");
    else if (n == 2)
        printf("Jämt\n");
    else if (n > 2)
        even_or_odd(n - 2);

}
