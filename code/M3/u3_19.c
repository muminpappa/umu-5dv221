#include <stdio.h>

int read_number(const int n);
void swap_int(int *n_1, int *n_2);
void sort(int *n_1, int *n_2);

int main(void)
{

    int number_1, number_2;

    number_1 = read_number(1);
    number_2 = read_number(2);
    
    printf("Osorterad: number_1 = %d, number_2 = %d\n",
           number_1, number_2);
    
    sort(&number_1, &number_2);

    printf("Efter sorteringen: number_1 = %d, number_2 = %d\n",
           number_1, number_2);

    return 0;

}

int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}

void swap_int(int *n_1, int *n_2)
{

    int temp = *n_1;
    *n_1 = *n_2;
    *n_2 = temp;

}

void sort(int *n_1, int *n_2)
{

    if (*n_1 > *n_2){
        swap_int(n_1, n_2);
    }

}
