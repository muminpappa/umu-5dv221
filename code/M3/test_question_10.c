#include <stdio.h>

void boom_bang(int n);
int read_number(const int n);

int main (void)
{
    int n = read_number(1);
    boom_bang(n);
    return 0;
}

void boom_bang(int n)
{

    if ( n % 15 == 0 )
    {
        printf("boom bang\n");
    } else if (n % 3 == 0) {
        printf("boom\n");
    } else if (n % 5 == 0) {
        printf("bang\n");
    } else {
        printf("%d\n", n);
    }
}


int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}
