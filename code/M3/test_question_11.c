#include <stdio.h>

void boom_bang(int n);
int read_number(const int n);

int main (void)
{
    int n = read_number(1);
    boom_bang(n);
    return 0;
}

void boom_bang(int n)
{
    switch ( n % 15 ){
        case 0: printf("boom bang\n"); break;
        case 3:
        case 6:
        case 9:
        case 12: printf("boom\n"); break;
        case 5:
        case 10: printf("bang\n"); break;
        default: printf("%d\n", n);
    }

}


int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}
