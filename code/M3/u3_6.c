#include <stdio.h>

int main(void)
{

    int number_1, number_2;

    printf("Skriv in ett heltal: "); 
    scanf("%d", &number_1);

    printf("Skriv in ett till heltal: "); 
    scanf("%d", &number_2);

    if (number_1 < number_2){
        printf ("Talet %d är mindre än %d.\n",
                number_1, number_2);
    } else if (number_1 > number_2){
        printf ("Talet %d är större än %d.\n",
                number_1, number_2);
    } else {
        printf ("Talen är lika stora.\n");
    }
    
    return 0;

}
