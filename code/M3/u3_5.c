#include <stdio.h>

int main(void)
{

    int weight;

    printf("Vikt: ");
    scanf("%d", &weight);

    if (weight>5){
        printf ("Vikten större än 5.\n");
    } else {
        printf ("Vikten mindre än eller lika med 5.\n");
    }
    
    return 0;

}
