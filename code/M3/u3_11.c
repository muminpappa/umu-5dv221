/* Programmering i C och Matlab

   Fil: plus_one_bad.c
   Författare: Jonny Pettersson
   Användare: jonny
   Datum: 9 september 2018
   Beskrivning:
   Ett exempel på "oläslig kod".
   Ett enkelt program för att visa utskrift,
   inläsning, variabler och tilldelning till variabler.
   Input: Ett heltal.
   Output: Heltalet närmast efter det inlästa.
   Begränsning: Det finns ingen kontroll av input.
*/

#include <stdio.h>

int main(void)
{
    int nisse;

    /* Läs in ett heltal från användaren */
    printf("Skriv in ett heltal > ");
    scanf("%d", &nisse);

    /* Lägg till 1 till talet */
    nisse = nisse + 1;

    /* Skriv ut det nya talet */
    printf("Talet efter ditt tal är %d\n", nisse);

    return 0;

}
