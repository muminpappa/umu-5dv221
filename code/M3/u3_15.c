#include <stdio.h>

int compare(const int n_1, const int n_2);
int read_number(const int n);

int main(void)
{

    int number_1, number_2, comparison;

    number_1 = read_number(1);
    number_2 = read_number(2);

    comparison = compare(number_1, number_2);

    switch (comparison){
    case -1: printf("Tal 1 är minst\n"); break;
    case 1:  printf("Tal 2 är minst\n"); break;
    case 0:  printf("Talen är lika stora\n"); break;
    }

    return 0;
}

int compare(const int number_1, const int number_2)
{
    int result = 0;
    if (number_1 < number_2) {
        result = -1;
    }
    else if (number_2 < number_1) {
        result = 1;
    }
    return result;
}

int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}
