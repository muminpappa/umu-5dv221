#include <stdio.h>

int sum(int n);
int read_number(const int n);

int main (void)
{

    int n;

    n = read_number(1);

    printf("%d\n", sum(n));
    return 0;
}

int sum(int n)
{
    int rv = 0;

    if (n > 0){
        rv = n + sum(n-1);
    }
    return rv;
}

int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}
