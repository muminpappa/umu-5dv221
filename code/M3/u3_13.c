#include <stdio.h>

int main (void)
{

    int value;

    printf("1: Coca cola\n");
    printf("2: Fanta\n");
    printf("3: Sprite\n");
    printf("4: Coca Cola Light\n");

    printf("Välj din läsk > ");
    scanf("%d", &value);

    switch(value) {
    case 1: printf("Du valde 1: Coca cola\n");
        break;
    case 2: printf("Du valde 2: Fanta\n");
        break;
    case 3: printf("Du valde 3: Sprite\n");
        break;
    case 4: printf("Du valde 4: Coca Cola Light\n");
        break;
    default: printf("Ogiltigt val\n");
    }

    return 0;

}
