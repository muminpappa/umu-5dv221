#include <stdio.h>

int read_number(const int n);
void order(int *small, int *medium, int *large); 
void swap(int *n_1, int *n_2);

int main(void)
{

    int number_1, number_2, number_3;

    number_1 = read_number(1);
    number_2 = read_number(2);
    number_3 = read_number(3);
        
    order(&number_1, &number_2, &number_3);

    printf("number_1 = %d, number_2 = %d, number_3 = %d\n",
           number_1, number_2, number_3);

    return 0;

}

int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}

void order(int *small, int *medium, int *large)
{
    if(*small > *medium)
        swap(small, medium);
    if(*medium > *large)
        swap(medium, large);
    if(*small > *medium)
        swap(small, medium);
}

void swap(int *n_1, int *n_2)
{
    int temp = *n_1;
    *n_1 = *n_2;
    *n_2 = temp;
}
