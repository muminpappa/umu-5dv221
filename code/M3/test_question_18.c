#include <stdio.h>

int cumsum(int n);
int read_number(const int n);

int main (void)
{
    int n = read_number(1);
    printf("Summa: %d\n", cumsum(n));
    return 0;
}

int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}

int cumsum(int n)
{
    int rv = 0;
    if (n == 1)
        rv = 1;
    else if (n > 1)
        rv = n + cumsum(n - 1);
    return rv;
}
