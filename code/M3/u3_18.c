#include <stdio.h>

int read_number(const int n);
 
int max(int number_1, int number_2);
int max_3(int number_1, int number_2, int number_3);

int main(void)
{
    int number_1, number_2, number_3, max_number;
    
    number_1 = read_number(1);
    number_2 = read_number(2);
    number_3 = read_number(3);

    max_number = max_3(number_1, number_2, number_3);
    
    printf("%d är störst.\n", max_number);
    
    return 0;
}

int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}

int max(int n_1, int n_2)
{
    int rv = n_1;

    if (n_2 > rv){
        rv = n_2;
    }
    
    return rv;
}

int max_3(int n_1, int n_2, int n_3)
{
    int rv = max(n_1, n_2);
    rv = max(rv, n_3);
    return rv;
}
