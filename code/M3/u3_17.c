#include <stdio.h>

_Bool equal(int number_1, int number_2);
_Bool unequal(int number_1, int number_2);
int read_number(const int n);


int main(void)
{
    int number_1 = read_number(1);
    int number_2 = read_number(2);

    printf("Talen är %s\n",
           unequal(number_1,number_2)==1?"olika":"lika");

    return 0;
}

_Bool equal(int number_1, int number_2)
{
    int rv = 0;
    if (number_1 == number_2)
        rv = 1;
    return rv;
}

_Bool unequal(int number_1, int number_2)
{
    return ! equal(number_1, number_2);
}


int read_number(const int n)
{
    int number;
    printf("Mata in tal %d > ", n);
    scanf("%d", &number);
    return number;
}
