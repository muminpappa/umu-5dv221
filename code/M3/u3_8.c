#include <stdio.h>

int main(void)
{
   int age;

   age = 17;

   if (age > 20)
      printf("Min ålder är ");
      printf("större än 20 år\n");

/*       u3_8.c: In function ‘main’: */
/* u3_8.c:9:4: warning: this ‘if’ clause does not guard... [-Wmisleading-indentation] */
/*     9 |    if (age > 20) */
/*       |    ^~ */
/* u3_8.c:11:7: note: ...this statement, but the latter is misleadingly indented as if it were guarded by the ‘if’ */
/*    11 |       printf("större än 20 år\n"); */
/*       |       ^~~~~~ */


   return 0;
}
