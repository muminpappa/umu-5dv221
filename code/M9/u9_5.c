#include <stdio.h>
#include <string.h>

#define LEN 20

int main(void)
{
    char str1[LEN], str2[LEN];
    int cmp;

    printf("Enter string 1: ");
    fgets(str1, LEN, stdin);
    printf("Enter string 2: ");
    fgets(str2, LEN, stdin);

    cmp = strncmp(str1, str2, LEN);
    
    if(cmp == 0) {
        printf("str1 and str2 are equal\n");
    } else if (cmp < 0) {
        printf("str1 comes first\n");
    } else if (cmp > 0) {
        printf("str2 comes first\n");
    }

    return 0;
}
