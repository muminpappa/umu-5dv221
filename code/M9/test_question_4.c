#include <stdio.h>

void write_array_to_file(const char filename[], int n, const int a[]);

int main(void)
{

    const int arr[10] = {1,2,3,4,5,6,7,8,9,0};
    write_array_to_file("array.log", 10, arr);

    return 0;
}

void write_array_to_file(const char filename[], int n, const int a[])
{
    FILE *fp = fopen(filename, "r");
    if (fp != NULL) {
        fprintf(fp, "%d ", n);
        for (int i = 0; i < n; i++) {
            fprintf(fp, "%d ", a[i]);
        }
    fclose(fp);
    }
}
