#include <stdio.h>
#include <stdlib.h>

int **alloc_matrix(int rows, int columns);
void fill_matrix(int **m, int rows, int columns);
void print_matrix(int **m, int rows, int columns);
void free_matrix(int **m, int rows);

int main(int argc, char *argv[])
{
    int **m;
    int rows, columns;

    if (argc >= 3) {
        sscanf(argv[1], "%d", &rows);
        sscanf(argv[2], "%d", &columns);


        m = alloc_matrix(rows, columns);
        fill_matrix(m, rows, columns);
        print_matrix(m, rows, columns);
        free_matrix(m, rows);

        return 0;
    }
    else {
        return 1;
    }
}

/* Dynamiskt allokerar en två-dimensionell array av heltal (int) */
int **alloc_matrix(int rows, int columns)
{
    int **m;

    m = calloc(rows, sizeof(int*));

    for (int i = 0; i < rows; i++) {
        m[i] = calloc(columns, sizeof(int));
    }

    return m;
}

/* Fyller arrayen med hjälp av två nästlade for-looper (dvs en
   for-loop i en annan for-loop) där summan av de två
   loopvariablerna stoppas in (dvs. m[i][j] = i + j;)
*/
void fill_matrix(int **m, int rows, int columns)
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            m[i][j] = i + j;
        }
    }
}

/* Skriver ut innehållet i arrayen */
void print_matrix(int **m, int rows, int columns)
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++ ) {
            printf("%4d", m[i][j]);
        }
        printf("\n");
    }
}

/* Lämnar tillbaka dynamiskt allokerat minne */
void free_matrix(int **m, int rows)
{
    for (int i = 0; i < rows; i++) {
        free(m[i]);
    }
    free(m);
}
