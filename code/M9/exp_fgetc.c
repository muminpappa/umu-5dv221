#include <stdio.h>

int main(int argc, const char *argv[])
{
    char c;
    FILE *fp = fopen(argv[1], "r");

    if (fp == NULL)
        return 1;

    while (c != EOF) {
        c = (char) fgetc(fp);
        printf("%c ", c);
    }

    printf("\n\n%d\n", EOF);

    return 0;
}
