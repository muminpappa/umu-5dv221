#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    if (argc >= 3) {
        int *n1, *n2;
        n1 = (int*) malloc(sizeof(int));
        n2 = (int*) malloc(sizeof(int));
        sscanf(argv[1], "%d", n1);
        sscanf(argv[2], "%d", n2);

        printf("%d + %d = %d\n", *n1, *n2, *n1 + *n2);
        
        free(n1);
        free(n2);
    }
    
    return 0;
}
