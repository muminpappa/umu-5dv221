#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    if (argc >= 3) {
        double *n1, *n2;
        n1 = (double*) malloc(sizeof(double));
        n2 = (double*) malloc(sizeof(double));
        sscanf(argv[1], "%lf", n1);
        sscanf(argv[2], "%lf", n2);

        printf("%g + %g = %g\n", *n1, *n2, *n1 + *n2);
        
        free(n1);
        free(n2);
    }
    
    return 0;
}
