#include <stdio.h>
#include <string.h>

#define LEN 20

int main(void)
{
    char str[LEN];

    fgets(str, LEN, stdin);

    for (int i = strlen(str) - 1; i >= 0; i--) {
        putchar(str[i]);
    }

    return 0;
}
