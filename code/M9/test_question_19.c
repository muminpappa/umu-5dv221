#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void)
{
    typedef struct {
        int n;
        char *p;
    } s;

    s *sp = malloc(sizeof(s));

    sp->p = malloc(5*sizeof(char));
    strncpy(sp->p, "Axel", 5);
    printf("%s\n", (*sp).p);

    sp->n = 5;
    printf("%d\n", (*sp).n);

    free(sp->p);
    free(sp);

    return 0;
}
