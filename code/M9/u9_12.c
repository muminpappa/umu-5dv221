#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    double *arr;
    int n;

    /* Frågar användaren om hur många värden som ska läsas in och läser in svaret */
    printf("Number of values: ");
    scanf("%d", &n);
    
    /* Dynamiskt allokerar minne för en array med tal av typen double,
       antalet tal är det som användaren angav */
    arr = (double*) calloc(n, sizeof(double));
    
    /* Läser in värden från användaren till alla element i arrayen */
    for (int i = 0; i < n; i++) {
        printf("Value of a[%d]: ", i);
        scanf("%lf", &arr[i]);
    }

    printf("\n");
    
    /* Skriver ut alla värden i arrayen */
    for (int i = 0; i < n; i++) {
        printf("a[%d] = %g\n", i, arr[i]);
    }

    /* Avallokerar det dynamiskt allokerade minnet */
    free(arr);
    
    return 0;
}
