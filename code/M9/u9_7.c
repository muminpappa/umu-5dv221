#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define LEN 20

_Bool is_palindrome(int n, const char str[]);
int clean_string(int n, const char in[], char out[]);


int main(void)
{
    char str[LEN];

    printf("Enter a string: ");
    fgets(str, LEN, stdin);

    printf("String '%s' is %s a palidrome.\n", str,
           is_palindrome(LEN, str) == 0 ? "not" : "\b");
    
    return 0;
}

_Bool is_palindrome(int l, const char str[])
{
    _Bool rv = 1;
    char clean[LEN];
    int n;
    clean_string(l, str, clean);
    n = strnlen(clean, LEN);
    printf("'%s' is %d characters long\n", clean, n);
    
    for (int i = 0; i < (n - 1) / 2 && rv; i++) {
        rv = clean[i] == clean[n-i-1] ? 1 : 0;
    }

    
    return rv;
}

int clean_string(int n, const char in[], char out[])
{
    int j = 0;
    for (int i = 0; i < n && in[i] != 0; i++) {
        if (isalpha(in[i])) {
            out[j] = tolower(in[i]);
            j++;
        }
    }
    return j;
}
