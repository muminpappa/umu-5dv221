#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int *random_array(int n);

int main(void)
{
    int *a = random_array(3);

    for (int *ap = a; ap <= &a[2]; ap++)
        printf("%d ", *ap);

    return 0;
}

int *random_array(int n)
{
    int *a = (int*) calloc(n, sizeof(int));
    srand(time(NULL));
    for (int i = 0; i < n; i++) {
        a[i] = rand() % 100;
    }

    return a;
}
