#include <stdio.h>
#include <ctype.h>

void count_long_lines(char filename[]);

int main(void)
{

    count_long_lines("mixed.log");
    
    return 0;
}

void count_long_lines(char filename[])
{
    FILE *fp = fopen(filename, "r");
    int long_lines = 0;
    int c, i = 0;
    if (fp != NULL) {
        do {
            c = fgetc(fp);
            if ((char) c == '\n' || (char) c == '\r') {
                long_lines += (i > 80) ? 1 : 0;
                i = 0;
                }
            else {
            i++;
            }
        } while (c != EOF);
    printf("%d lines are longer than 80 characters.\n", long_lines);
    fclose(fp);
    }
}
