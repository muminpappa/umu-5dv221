#include <stdio.h>
#include <stdlib.h>

char *read_line(void);

int main(void)
{
    char *str = read_line();

    printf("%s\n", str);

    return 0;
}

char *read_line(void)
{
    char *str = NULL, c;
    int n = 0;
    do {
        c = (char) getchar();
        str = realloc(str, (n + 1) * sizeof(char));
        str[n] = c;
        n++;
    } while (c != 0xa);
    // terminate string
    str[n - 1] = 0;
    return str;
}
