#include <stdio.h>
#include <stdlib.h>

char *read_line(void);

int main(void)
{
    char *str = read_line();

    printf("%s\n", str);

    return 0;
}

char *read_line(void)
{
    char *str = NULL, c;
    int n = 0;
    do {
        char *tmp = malloc((n + 1) * sizeof(char));
        c = (char) getchar();
        tmp[n] = c;
        for (int i = 0; i < n; i++) {
            tmp[i] = str[i];
            }
        n++;
        free(str);
        str = tmp;
        } while (c != 0xa);
    str[n-1] = 0;
    return str;
}
