#ifndef _TEST_QUESTION_7_H
#define _TEST_QUESTION_7_H

#include <stdlib.h>

void read_array_from_file(const char filename[], int *n, int arr[]);

#endif
