#include <stdio.h>
#include "test_question_7.h"

#define MAX_SIZE 100

int main(void)
{

    int arr[MAX_SIZE];
    int n;
    read_array_from_file("array.log", &n, arr);

    printf("%d\n", n);

    for (int i = 0; i < n; i++)
        printf("%d\n", arr[i]);

    return 0;
}

void read_array_from_file(const char filename[], int *n, int arr[])
{
    FILE *fp = fopen(filename, "r");
    if (fp != NULL) {
        int nr_of_elements;
        fscanf(fp, "%d", &nr_of_elements);
        *n = nr_of_elements;
        for (int i = 0; i < nr_of_elements && i < MAX_SIZE; i++) {
            fscanf(fp, "%d", &arr[i]);
        }
    fclose(fp);
    }
}
