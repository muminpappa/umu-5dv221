#include <stdio.h>
#include <stdlib.h>

typedef struct {
    double length;
    double width;
    double height;
} box;

int main(void)
{
    box *b = malloc(sizeof(box));

    (*b).length = 15.0;
    b->width = 6.0;
    b->height = 9.5;

    printf("Volume = %g\n", (*b).length *  (*b).width *  (*b).height);

    free(b);

    return 0;
}
