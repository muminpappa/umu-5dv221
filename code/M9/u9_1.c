#include <stdio.h>

int main(void)
{
    int n = 0;

    while (++n && getchar() != '\n');
    printf("%d", n);

    return 0;
}
