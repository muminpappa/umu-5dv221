#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int *intp;

    intp = (int*) malloc(sizeof(int));

    *intp = 7;

    printf("intp points on memory with value %d\n", *intp);

    free(intp);

    return 0;
}
