#include <stdio.h>
#include <stdlib.h>

int **read_matrix(const char *file);

int main(void)
{
    int **arr = read_matrix("matrix.log");

    printf("%d\n", arr[0][0]);

    return 0;
}

int **read_matrix(const char *file)
{
    int **m;
    FILE *fp = fopen(file, "r");
    int rows, cols;

    if (fp == NULL) {
        return NULL;
    }

    fscanf(fp, "%d %d", &rows, &cols);

    m = (int**) calloc(rows, sizeof(int*));
    for (int i = 0; i < rows; i++) {
        m[i] = (int*) calloc(cols, sizeof(int));
        for (int j = 0; j < cols; j++) {
            fscanf(fp, "%d", &m[i][j]);
        }
    }

    fclose(fp);

    return m;
}
