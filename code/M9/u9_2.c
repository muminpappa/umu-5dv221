#include <stdio.h>

#define STRLENGTH 20

int main(void)
{
    char str[STRLENGTH];
    int i = 0;
    char a;
    
    do {
        a = getchar();
        if (a != EOF) {
            str[i++] = a;
        }

    } while (++i < STRLENGTH - 1 && a != EOF);
        
    
    printf("%s\n", str);
    
    return 0;
}
