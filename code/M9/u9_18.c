#include <stdio.h>
#include <stdlib.h>

double *alloc_array(int n);
void read_values(double a[], int n);
void print_array(const double a[], int n);
void free_array(double a[]);
void sort_array(double a[], int n);
void swap(double *v1, double *v2);

int main(void)
{
    double *arr;
    int n;

    printf("Number of values: ");
    scanf("%d", &n);
    
    arr = alloc_array(n);
    
    read_values(arr, n);
    
    printf("\nBefore sorting:\n");
    print_array(arr, n);

    sort_array(arr, n);

    printf("\nAfter sorting:\n");
    print_array(arr, n);
        
    free_array(arr);
    
    return 0;
}

double *alloc_array(int n)
{
    return (double*) calloc(n, sizeof(double));
}

void read_values(double a[], int n)
{
    for (int i = 0; i < n; i++) {
        printf("Value of a[%d]: ", i);
        scanf("%lf", &a[i]);
    }
}

void print_array(const double a[], int n)
{
    printf("\n");
    for (int i = 0; i < n; i++) {
        printf("a[%d] = %g\n", i, a[i]);
    }

}

void free_array(double a[])
{
    free(a);
}

void sort_array(double a[], int n)
{
    for (int i = 1; i < n; i++) {
        for (int j = 1; j < n; j++) {
            if (a[j-1] > a[j]) {
                swap(&a[j-1], &a[j]);
            }
        }
    }
}

void swap(double *v1, double *v2)
{
    double temp = *v1;
    *v1 = *v2;
    *v2 = temp;
}
