#include <stdio.h>
#include <stdlib.h>

int *read_matrix(const char *file);
void print_matrix(int rows, int cols, int m[rows][cols]);

int main(void)
{
    int *arr = read_matrix("matrix.log");

    print_matrix(2, 2, arr);
    
    return 0;
}

int *read_matrix(const char *file)
{
    int *m;
    FILE *fp = fopen(file, "r");
    int rows, cols;

    if (fp == NULL) {
        return NULL;
    }

    fscanf(fp, "%d %d", &rows, &cols);

    m = (int*) calloc(rows*cols, sizeof(int));
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            fscanf(fp, "%d", &m[i * rows + j]);
        }
    }

    fclose(fp);

    return m;
}

void print_matrix(int rows, int cols, int m[rows][cols])
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%4d", m[i][j]);
        }
        printf("\n");
    }
}
