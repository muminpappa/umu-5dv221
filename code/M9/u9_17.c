#include <stdio.h>
#include <stdlib.h>

typedef struct {
    double length;
    double width;
    double height;
} box;

box *allocate_box_array(int n);
void fill_box_array(box *bA, int n);
void read_box_values(box *b);
void print_box_array(box *bA, int n);
void print_box(box *b);
double calc_volume(box *b);
void free_box_array(box *bA);

int main(void)
{
    box *bA;
    int number_of_boxes;

    printf("Number of boxes: ");
    scanf("%d", &number_of_boxes);

    bA = allocate_box_array(number_of_boxes);

    fill_box_array(bA, number_of_boxes);
    
    print_box_array(bA, number_of_boxes);

    free_box_array(bA);

    return 0;
}

box * allocate_box_array(int n)
{
    return (box*) calloc(n, sizeof(box));
}

void fill_box_array(box *bA, int n)
{
    for (int i = 0; i < n; i++) {
        printf("\nValues for box %d\n", i+1);
        read_box_values(&bA[i]);
    }

}

void read_box_values(box *b)
{
    printf("Box length: ");
    scanf("%lf", &(*b).length);
    printf("Box width: ");
    scanf("%lf", &(*b).width);
    printf("Box height: ");
    scanf("%lf", &(*b).height);
}
    
double calc_volume(box *b)
{
    return (*b).length *  (*b).width *  (*b).height;
}

void print_box_array(box *bA, int n)
{
    for (int i = 0; i < n; i++) {
        printf("\nBox %d\n", i+1);
        print_box(&bA[i]);
        printf("\n");
    }
}

void print_box(box *b)
{
    printf("Length = %g, width = %g, height = %g, volume = %g\n",
           b->length, b->width, b->height, calc_volume(b));
}

void free_box_array(box *bA)
{
    free(bA);
}
