#include <stdio.h>
#include <stdlib.h>

char *copy_string(const char *s);


int main(int argc, const char *argv[])
{
    char *copy;

    if (argc != 2)
        return 1;

    copy = copy_string(argv[1]);
    printf("src: %s\n", argv[1]);
    printf("dst: %s\n", copy);

    return 0;
}

char *copy_string(const char *s)
{
    int n = 0;
    while (s[n++] != 0);

    char *rv = calloc(n, sizeof(char));
    for (int i = 0; i <= n; i++) {
        rv[i] = s[i];
    }

    return rv;
}
