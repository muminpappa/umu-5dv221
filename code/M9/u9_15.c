#include <stdio.h>
#include <stdlib.h>

typedef struct {
    double length;
    double width;
    double height;
} box;

box * allocate_box(void);
void get_box_size(box *b);
double calc_volume(box *b);

int main(void)
{
    box *b = allocate_box();

    get_box_size(b);
    
    printf("Volume = %g\n", calc_volume(b));

    free(b);

    return 0;
}

box * allocate_box(void)
{
    return (box*) malloc(sizeof(box));
}
    
void get_box_size(box *b)
{
    printf("Box length: ");
    scanf("%lf", &(*b).length);
    printf("Box width: ");
    scanf("%lf", &(*b).width);
    printf("Box height: ");
    scanf("%lf", &(*b).height);
}
    
double calc_volume(box *b)
{
    return (*b).length *  (*b).width *  (*b).height;
}
