#include <stdio.h>
#include <string.h>

void cat_args(int argc, const char *argv[]);

int main(int argc, const char *argv[])
{
    cat_args(argc, argv);
    return 0;
}

void cat_args(int argc, const char *argv[])
{
    const int n = 10;
    char str[n];
    const char div[] = "|";
    str[0] = 0;

    for (int i = 1;
         i < argc && strlen(str) + strlen(argv[i]) < n;
         i++) {
        strncat(str, argv[i], strlen(argv[i]));
        printf("str is %d bytes long\n", (int) strlen(str));
        if (i < argc - 1) {
            strncat(str, div, strlen(div));
        }
    }
    printf("%s\n", str);
}
