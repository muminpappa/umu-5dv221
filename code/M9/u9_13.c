#include <stdio.h>
#include <stdlib.h>

double *alloc_array(int n);
void read_values(double a[], int n);
void print_array(const double a[], int n);
void free_array(double a[]);

int main(void)
{
    double *arr;
    int n;

    /* Frågar användaren om hur många värden som ska läsas in och läser in svaret */
    printf("Number of values: ");
    scanf("%d", &n);
    
    /* Dynamiskt allokerar minne för en array med tal av typen double,
       antalet tal är det som användaren angav */
    arr = alloc_array(n);
    
    /* Läser in värden från användaren till alla element i arrayen */
    read_values(arr, n);
    
    /* Skriver ut alla värden i arrayen */
    print_array(arr, n);
        
    /* Avallokerar det dynamiskt allokerade minnet */
    free_array(arr);
    
    return 0;
}

double *alloc_array(int n)
{
    return (double*) calloc(n, sizeof(double));
}

void read_values(double a[], int n)
{
    for (int i = 0; i < n; i++) {
        printf("Value of a[%d]: ", i);
        scanf("%lf", &a[i]);
    }
}

void print_array(const double a[], int n)
{
    printf("\n");
    for (int i = 0; i < n; i++) {
        printf("a[%d] = %g\n", i, a[i]);
    }

}

void free_array(double a[])
{
    free(a);
}
