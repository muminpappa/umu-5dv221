#include <stdio.h>
#include <stdlib.h>


char *read_line(void)
{
    char *str, c;
    int n = 0;
    do {
        char *tmp;
        c = (char) getchar();
        n++;
        tmp = malloc(n * sizeof(char));
        for (int i = 0; i < n - 1; i++) {
            tmp[i] = str[i];
            }
        free(str);
        str = tmp;
        } while (c != 0xa);
    return str;
}
