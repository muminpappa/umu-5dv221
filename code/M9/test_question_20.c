#include <stdio.h>
#include <stdlib.h>

double **alloc_matrix(int rows, int columns);
void sum_and_no_of_neg(double **array, int rows, int columns, double
    *sum_neg, int *no_of_neg);
void free_matrix(double **matrix, int rows);

int main(void)
{
    int rows = 2, columns = 3, no_of_neg;
    double **m = alloc_matrix(rows, columns), sum_neg;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            m[i][j] = i + j - 2;
        }
    }

    sum_and_no_of_neg(m, rows, columns, &sum_neg, &no_of_neg);

    free_matrix(m, rows);

}

double **alloc_matrix(int rows, int columns)
{
    double **m = calloc(rows, sizeof(double*));
    for (int i = 0; i < rows; i++) {
        m[i] = calloc(columns, sizeof(double));
    }

    return m;
}


void sum_and_no_of_neg(double **array, int rows, int columns, double
    *sum_neg, int *no_of_neg)
{
    *sum_neg = 0;
    *no_of_neg = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            *sum_neg += array[i][j] < 0 ? array[i][j] : 0;
            *no_of_neg += array[i][j] < 0 ? 1 : 0;
        }
    }

    printf("Sum of negative values: %g\n", *sum_neg);
    printf("Number of negative values: %d\n", *no_of_neg);
}

void free_matrix(double **matrix, int rows)
{
    for (int i = 0; i < rows; i++) {
        free(matrix[i]);
    }
    free(matrix);
}
