#include <stdio.h>
#include <string.h>

#define LEN 20

int main(void)
{
    char str1[LEN], str2[LEN];
    int cmp;

    printf("Enter string 1: ");
    fgets(str1, LEN, stdin);
    printf("Enter string 2: ");
    fgets(str2, LEN, stdin);

    cmp = strncmp(str1, str2, LEN);
    
    if(cmp == 0) {
        printf("str1 and str2 are equal\n");
    }
    else {
        printf("String %d comes first\n",
               cmp < 0 ? 1 : 2);
    }

    return 0;
}
