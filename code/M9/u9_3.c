#include <stdio.h>

#define STRLENGTH 20

int main(void)
{
    char str[STRLENGTH];
    int i = 0;
    
    do {
        str[i] = getchar();
        i++;
    } while (i < STRLENGTH - 1 && str[i-1] != '\n');
        
    str[i]=0;
    printf("%s", str);
    
    return 0;
}
