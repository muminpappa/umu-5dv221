#include <stdio.h>
#include <stdlib.h>

int *read_array(const char *file);

int main(void)
{
    int *arr = read_array("array.log");

    printf("%d\n", arr[0]);

    return 0;
}

int *read_array(const char *file)
{
    int *a, n;
    FILE *fp = fopen(file, "r");

    if (fp == NULL) {
        return NULL;
    }

    fscanf(fp, "%d", &n);
    a = calloc(n, sizeof(int));
    for (int i = 0; i < n; i++) {
        fscanf(fp, "%d", &a[i]);
    }
    fclose(fp);

    return a;
}
