#include <stdio.h>
#include <ctype.h>

void upcase_string(int n, char str[]);

int main(void)
{
    const int n = 100;
    char str[n];

    printf("Enter a string: ");
    fgets(str, n, stdin);

    upcase_string(n, str);

    printf("%s\n", str);

    return 0;
}

void upcase_string(int n, char str[])
{
    int i = 0;
    while (str[i] != 0 && i < n) {
        str[i] = toupper(str[i]);
        i++;
    }
}
