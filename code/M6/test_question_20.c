#include <stdio.h>

#define N 20

void print_backwards(char str[]);

int main(void)
{
    char str[N] = "Aprikosen";

    print_backwards(str);
    
    return 0;
}

void print_backwards(char str[])
{
    int n = 0;
    while (str[n]) {
        n++;
    }
    for (int i = n - 1; i >= 0; i--) {
        printf("%c\n", str[i]);
    }
}
