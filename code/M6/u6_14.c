/* add beräknar summan av sina två argument och ger den som
   returvärde */

/* sum tar en array av heltal och arrayns längd som argument. Med
   hjälp av funktionen add summerar den alla element i arrayn och ger
   danna summa som returvärde*/

#include <stdio.h>

int add(int a, int b);
int sum(int n, int numbers[]);
int main(void)

{
    int arr[5];
    for (int i = 0; i < 5; i++) {
        arr[i] = i + 1;
    }
    printf("Sum of numbers 1 to 5 is %d.\n", sum(5, arr));
    return 0;
}

int add(int a, int b)
{
    return a + b;
}

int sum(int n, int numbers[])
{
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum = add(sum, numbers[i]);
    }
    return sum;
}
