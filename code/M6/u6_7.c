#include <stdio.h>
#include <errno.h>

#define N 3

int main(void)
{

    float result[N];
    float number1[N] = {1.1, -0.5, 2.3};
    float number2[N] = {0.2, 0.5, -1.3};
    float sum;

    for (int i = 0; i < N; i++) {
        result[i] = number1[i] * number2[i];
    }

    sum = 0;
    for (int i = 0; i < N; i++) {
        sum += result[i];
    }
    printf("sum = %f\n", sum);

    for (int i = 0; i < N; i++) {
        printf("result[%d] = %f\n", i, result[i]);
    }

    return 0;
}
