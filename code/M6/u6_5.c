#include <stdio.h>
#include <errno.h>


int main(void)
{

    const int n = 5; 
    float a[n], m;

    for (int i = 0; i < n; i++) {
        printf("Enter value %d: ", i);
        scanf("%f", &a[i]);
    }

    m = a[0];
    
    printf("\n\n");
    
    for (int i = 0; i < n; i++) {
        m = a[i] > m ? a[i] : m;
    }

    printf("Maxvärdet är %f\n", m);
    
    return 0;
}
