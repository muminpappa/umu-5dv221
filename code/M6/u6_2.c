#include <stdio.h>

int main(void)
{
    const int n = 10;
    int a[n];

    for (int i = 0; i < n; i++) {
        a[i] = i + 5;
    }

    for (int i = 0; i < n; i++) {
        printf("I element %d av arr står det %d.\n",
               i, a[i]);
    }

    return 0;
}
