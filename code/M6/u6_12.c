#include <stdio.h>

#define N 3

void multiply_array_elements(int n, double f1[], double f2[], double res[]);
double sum_array(int n, double array[]);
void print_array(int n, double array[]);

int main(void)
{
    double result[N];
    double number1[N] = {1.1, -0.5, 2.3};
    double number2[N] = {0.2, 0.5, -1.3};

    multiply_array_elements(N, number1, number2, result);
    printf("sum = %f\n", sum_array(N, result));
    print_array(N, result);

    return 0;
}

void multiply_array_elements(int n, double f1[], double f2[], double res[])
{
    for (int i = 0; i < n; i++) {
        res[i] = f1[i] * f2[i];
    }
}


double sum_array(int n, double array[])
{
    double sum = 0;
    for (int i = 0; i < n; i++) {
        sum += array[i];
    }

    return sum;
}
    
void print_array(int n, double array[])
{
    for (int i = 0; i < n; i++) {
        printf("result[%d] = %lf\n", i, array[i]);
    }
}
