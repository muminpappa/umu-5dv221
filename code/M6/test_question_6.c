#include <stdio.h>
#include <stdlib.h>

double sum_over_array(int n, const double array[]);

int main(void)
{
    double *a;
    int n = 0;
    double sum;

    printf("Arrayns längd: ");
    scanf("%d", &n);

    a = (double*) malloc(n * sizeof(double));

    for (int i = 0; i < n; i++){
        printf("Element %d: ", i + 1);
        scanf("%lf", &a[i]);
    }

    sum = sum_over_array(n, a);

    printf("Summan är %g\n", sum);

    free(a);
    return 0;
}

double sum_over_array(int n, const double array[])
{
    double sum;
    /* basfall */
    if (n == 1){
        sum = array[0];
    }
    else if (n == 2) {
        sum = array[0] + array[1];
    }
    /* rekursivt fall */
    else {
        sum = sum_over_array(n/2, array) + sum_over_array(n - n/2, &array[n/2]);
    }
    return sum;
}
