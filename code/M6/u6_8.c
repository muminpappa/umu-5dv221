#include <stdio.h>

#define N 11

int main(void)
{
    int hist[N];
    int n;
    
    for (int i = 0; i < N; i++) {
        hist[i] = 0;
    }
    
    do {
        printf("Ange ett heltal (0 - 10): ");
        scanf("%d", &n);
        if ( n < N && n >= 0 ) {
            hist[n]++;
        }
    } while ( n < N && n >= 0 );

    printf("\n");
        
    for (int i = 0; i < N; i++) {
        printf("Det fanns %2d tal med värdet %d\n", hist[i], i);
    }
    
    return 0;
}
