#include <stdio.h>

#define N 20

void delete_vowels(int n, char str[]);

int main(void)
{
    char str[N] = "Aprikosenmarmeladen";
    delete_vowels(N, str);
    printf("%s\n", str);

    return 0;
}

void delete_vowels(int n, char str[])
{
    int j = 0;
    for (int i = 0; i < n && str[i] != 0; i++) {
        switch(str[i]){
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
        case 'A':
        case 'E':
        case 'I':
        case 'O':
        case 'U':
            break;
        default:
            str[j] = str[i];
            j++;
            break;
        }
    }
    str[j] = 0;
}
