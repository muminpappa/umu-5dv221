#include <stdio.h>
#include <stdlib.h>

_Bool is_sorted_ascended(int n, const double a[]);

int main(void)
{
    double *a;
    int n = 0;

    printf("Arrayns längd: ");
    scanf("%d", &n);

    a = (double*) malloc(n * sizeof(double));

    for (int i = 0; i < n; i++){
        printf("Element %d: ", i + 1);
        scanf("%lf", &a[i]);
    }

    if (is_sorted_ascended(n, a)){
        printf("True\n");
    }
    else {
        printf("False\n");
    }
    
    free(a);
    return 0;
}

_Bool is_sorted_ascended(int n, const double a[])
{
    _Bool sorted = 1;
    int i = 1;
    while (sorted && i < n){
        if (a[i] < a[i-1]){
            sorted = 0;
        }
        i++;
    }
    return sorted;
}
