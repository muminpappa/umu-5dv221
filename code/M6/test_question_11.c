#include <stdio.h>

#define N 20

void copy_str(int n, const char src[], char dest[]);

int main(void)
{
    char src[N] = "Hallo", dest[N];

    copy_str(N, src, dest);
    printf("%s\n", dest);
    
    return 0;
}

void copy_str(int n, const char src[], char dest[])
{
    int i = 0;
    do {
        dest[i] = src[i];
        i++;
    } while (src[i] != 0 && i < n);
}
