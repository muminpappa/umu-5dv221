#include <stdio.h>

#define N 10

void print_array(int n, int arr[]);

int main(void)
{
    int a[N] = {1,2,3,4,5,6,7,8,9,10};

    print_array(N, a);
    return 0;
}

void print_array(int n, int arr[])
{
    /* Basfall */
    if (n == 1) {
        printf("%d ", arr[n - 1]);
    }
    /* Rekursivt fall */
    else {
        printf("%d ", arr[0]);
        print_array(n - 1, &arr[1]);
    }
}
