#include <stdio.h>

void read_array(int n, double array[]);
double get_max_in_array(int n, const double array[]);

int main(void)
{
    const int n = 5;
    double a[n];

    read_array(n, a);

    printf("\n\n");

    printf("Maxvärdet är %f\n",
           get_max_in_array(n, a));

    return 0;
}

void read_array(int n, double array[])
{
    for (int i = 0; i < n; i++) {
        printf("Enter value %d: ", i + 1);
        scanf("%lf", &array[i]);
    }
}

double get_max_in_array(int n, const double array[])
{
    double m = array[0];
    for (int i = 0; i < n; i++) {
        m = array[i] > m ? array[i] : m;
    }

    return m;
}
