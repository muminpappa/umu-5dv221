#include <stdio.h>

int main(void)
{

    const int n = 5; 
    int a[n];

    for (int i = 0; i < n; i++) {
        printf("Enter value %d: ", i);
        scanf("%d", &a[i]);
    }

    printf("\n\n");
    
    for (int i = 0; i < n; i++) {
        printf("I element %d av arr står det %d.\n",
               i, a[i]);
    }

    return 0;
}
