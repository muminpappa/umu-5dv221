#include <stdio.h>

#define N 20

void append_str(int n, const char src[], char dest[]);

int main(void)
{
    char src[N] = " World", dest[N] = "Hello";

    append_str(N, src, dest);
    printf("%s\n", dest);
    
    return 0;
}

void append_str(int n, const char src[], char dest[])
{
    int dest_end = 0, j = 0;
    /* Find end of string (\0) in dest */
    while (dest_end < n && dest[dest_end] != 0) {
        dest_end++;
    }
    do {
        dest[dest_end + j] = src[j];
        j++;
    } while (dest_end + j < n && src[j] != 0);
}
