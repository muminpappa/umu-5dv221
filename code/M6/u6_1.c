#include <stdio.h>

int main(void)
{
    const int n = 10;
    int a[n];

    a[0] = 10;
    a[1] = 11;
    a[2] = 12;
    a[3] = 13;
    a[4] = 14;
    a[5] = 15;
    a[6] = 16;
    a[7] = 17;
    a[8] = 18;
    a[9] = 19;

    for (int i = 0; i < n; i++) {
        printf("I element %d av arr står det %d.\n",
               i, a[i]);
    }

    return 0;
}
