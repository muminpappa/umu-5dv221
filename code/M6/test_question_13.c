#include <stdio.h>

#define N 20

void replace_vowels(int n, char str[]);
    
int main(void)
{
    char str[N] = "Abrikadebroum";
    replace_vowels(N, str);
    printf("%s\n", str);
    
    return 0;
}

void replace_vowels(int n, char str[])
{
    int i = 0;
    while (i < n && str[i] != 0){
        switch(str[i]){
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u': str[i] = ' '; break;
        }
        i++;
    }
}
