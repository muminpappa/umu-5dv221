#include <stdio.h>
#include <stdlib.h>

_Bool is_single_digit(int n, const int a[]);

int main(void)
{
    int *a;
    int n = 0;

    printf("Arrayns längd: ");
    scanf("%d", &n);

    a = (int*) malloc(n * sizeof(int));

    for (int i = 0; i < n; i++){
        printf("Element %d: ", i + 1);
        scanf("%d", &a[i]);
    }

    if (is_single_digit(n, a)){
        printf("True\n");
    }
    else {
        printf("False\n");
    }
    
    free(a);
    return 0;
}

_Bool is_single_digit(int n, const int a[])
{
    _Bool single = 1;
    int i = 0;
    while (single && i < n){
        if (a[i] < -9 || a[i] > 9){
            single = 0;
        }
        i++;
    }
    return single;
}
