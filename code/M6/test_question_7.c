#include <stdio.h>
#include <stdlib.h>

void zero_odds(int n, int array[]);

int main(void)
{
    int *a;
    int n = 0;

    printf("Arrayns längd: ");
    scanf("%d", &n);

    a = (int*) malloc(n * sizeof(int));

    for (int i = 0; i < n; i++){
        printf("Element %d: ", i + 1);
        scanf("%d", &a[i]);
    }

    zero_odds(n, a);

    for (int i = 0; i < n; i++) {
        printf("%d ", a[i]);
    }

    printf("\n");
    
    free(a);
    return 0;
}

void zero_odds(int n, int array[])
{
    for (int i = 0; i < n; i++){
        if (array[i] % 2){
            array[i] = 0;
        }
    }
}
