#include <stdio.h>

int foo(void)
{
    int n = 3;
    int arr[] = {7, 2, 5};

    for (int i = 1 ; i < n ; i++ ) {
        for (int j = 1 ; j < n ; j++) {
            if (arr[j-1] < arr[j]) {
                int temp = arr[j-1];
                arr[j-1] = arr[j];
                arr[j] = temp;
            }
        }
    }
    return arr[2];
}

int main(void)
{
    printf("arr[2] = %d\n", foo());

    return 0;
}
