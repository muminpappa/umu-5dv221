#include <stdio.h>

void fill_array(int n, int array[]);
void print_array(int n, int array[]);
int main(void)
{
    const int n = 10;
    int a[n];

    fill_array(n, a);
    print_array(n, a);
    
    return 0;
}

void fill_array(int n, int array[])
{
    for (int i = 0; i < n; i++) {
        array[i] = i + 5;
    }
}

void print_array(int n, int array[])
{
    for (int i = 0; i < n; i++) {
        printf("I element %d av arr står det %d.\n",
               i, array[i]);
    }
}
