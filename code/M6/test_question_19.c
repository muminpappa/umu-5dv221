#include <stdio.h>

double mean_of_matrix(int rows, int columns, const int array[rows][columns]);

int main(void)
{
    int a[3][2] = {{1,2},{4,5},{7,8}};
    printf("%g\n", mean_of_matrix(3, 2, a));
    return 0;
}

double mean_of_matrix(int rows, int columns, const int array[rows][columns])
{
    double sum = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            sum += (double) array[i][j];
        }
    }

    return sum / (rows * columns);
}
