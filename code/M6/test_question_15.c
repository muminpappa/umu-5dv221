#include <stdio.h>

#define N 20

void center_line(int n, const char str[]);

int main(void)
{
    char str[N] = "Apfel";
    center_line(N, str);

    return 0;
}

void center_line(int n, const char str[])
{
    int length = 0, i;
    const int width = 20;
    char padding[width/2];
    while (length < n && str[length] != 0) {
        length++;
    }
    for (i = 0; i < (width - length)/2; i++){
        padding[i] = ' ';
    }
    padding[i] = 0;

    printf("%s%s%s\n", padding, str, padding);
}

