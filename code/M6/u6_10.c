#include <stdio.h>

void read_array(int n, int array[]);
void print_array_forward(int n, int array[]);
void print_array_backward(int n, int array[]);

int main(void)
{

    const int n = 5; 
    int a[n];

    read_array(n, a);
    printf("\n\n");
    print_array_forward(n, a);
    printf("\n\n");
    print_array_backward(n, a);

    return 0;
}

void read_array(int n, int a[])
{
    for (int i = 0; i < n; i++) {
        printf("Enter value %d: ", i + 1);
        scanf("%d", &a[i]);
    }
}
    
void print_array_forward(int n, int a[])
{
    for (int i = 0; i < n; i++) {
        printf("I element %d av arr står det %d.\n",
               i, a[i]);
    }
}

void print_array_backward(int n, int a[])
{
    for (int i = n - 1; i >= 0; i--) {
        printf("I element %d av arr står det %d.\n",
               i, a[i]);
    }
}
