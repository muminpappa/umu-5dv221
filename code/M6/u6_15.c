#include <stdio.h>

int main(void)
{

    const int n = 5; 
    float a[n], max, min;

    for (int i = 0; i < n; i++) {
        printf("Enter value %d: ", i);
        scanf("%f", &a[i]);
    }

    max = min = a[0];
    
    printf("\n\n");
    
    for (int i = 0; i < n; i++) {
        max = a[i] > max ? a[i] : max;
        min = a[i] < min ? a[i] : min;
    }

    printf("Maxvärdet är %f\n", max);
    printf("Minvärdet är %f\n", min);
    
    return 0;
}
