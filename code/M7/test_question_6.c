struct circle {
    double r;
};

double area(const struct circle *circle )
{
    return 3.14159 * circle->r * circle->r;
}
