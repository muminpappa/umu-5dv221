#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void print_array(int n, double a[]);
void find_max_and_min(int length, double array[], double *max, double *min);

int main(void)
{

    const int n = 5; 
    double a[n], max, min;

    srand(time(NULL));

    for (int i = 0; i < n; i++) {
        a[i] = (rand() % 100) / 10.0 - 5.0;
    }

    print_array(n, a);

    printf("\n\n");

    find_max_and_min(n, a, &max, &min);
    printf("Maxvärdet är %g\n", max);
    printf("Minvärdet är %g\n", min);
    
    return 0;
}

void print_array(int n, double a[])
{
    for (int i = 0; i < n; i++) {
        printf("%g ", a[i]);
    }
    printf("\n");
}

void find_max_and_min(int length, double array[], double *max, double *min)
{
    *max = *min = array[0];
    
    for (int i = 0; i < length; i++) {
        *max = array[i] > *max ? array[i] : *max;
        *min = array[i] < *min ? array[i] : *min;
    }
}
