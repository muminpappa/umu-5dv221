#include <stdio.h>
typedef struct {
    double x;
    double y;
} point;
typedef struct {
    int id;
    point p1;
    point p2;
} line;
point create_point(double x_coord, double y_coord);
void print_point(point c);
point get_point();
line create_line(int id, point p1, point p2);
void print_line(line line1);
void print_line_array(line line_array[], int n);

int main(void)
{
    point p1 = {2.8, 1};
    point p2 = {3.7, 3.1};
    point p3 = {5.6, 8.2};
    line line1 = {1, p1, p2};
    line line2 = {2, p2, p3};
    const int n = 2;
    line line_array[n];
    line_array[0] = line1;
    line_array[1] = line2;
    print_line_array(line_array, n);
    line_array[0].p1.y = 67.56;
    line_array[1].p2.x = line_array[0].p1.y;
    line_array[1].id = 3;
    print_line_array(line_array, n);
    return 0;
}
point create_point(double x_coord, double y_coord)
{
    point p;
    p.x = x_coord;
    p.y = y_coord;
    return p;
}
void print_point(point p)
{
    printf("x = %.2f, y = %.2f\n", p.x, p.y);
}
line create_line(int id, point p1, point p2)
{
    line line1 = {id, p1, p2};
    return line1;
}
void print_line(line line1)
{
    printf("Line id: = %d\n", line1.id);
    printf("Point 1: ");
    print_point(line1.p1);
    printf("Point 2: ");
    print_point(line1.p2);
}
point get_point()
{
    point p;
    printf("Give x coordinate: ");
    scanf("%lf", &p.x);
    printf("Ange y coordinate: ");
    scanf("%lf", &p.y);
    return p;
}
void print_line_array(line line_array[], int n)
{
    for(int i = 0 ; i < n ; i++) {
        print_line(line_array[i]);
    }
}
