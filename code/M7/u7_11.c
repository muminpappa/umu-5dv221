#include "mydate.h"
#include <stdio.h>

date get_date(void);
void print_date(date d);

int main (void)
{
    date date;

    date = get_date();
    print_date(date);
    
    return 0;
}

date get_date(void)
{
    date date;
    printf("Enter number of year: ");
    scanf("%d", &date.year);
    printf("Enter number of month: ");
    scanf("%d", &date.month);
    printf("Enter number of day: ");
    scanf("%d", &date.day);

    return date;
}

void print_date(date d)
{
    printf("%02d%02d%02d\n", d.year % 100, d.month, d.day);
    
}
