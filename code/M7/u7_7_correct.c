#include <stdio.h>

#define SIZE 8

int max(int n, int a[]);

int main(void)
{
    int a[SIZE] = {3, 3, 4, 6, 7, 8, 9, 1};
    printf("Max = %d\n", max(SIZE, a));
    return 0;
}

int max(int n, int a[])
{
    int m = a[0];
    for (int i = 1 ; i < n ; i++) {
        if (a[i] > m) {
            m = a[i];
        }
    }
    return m;
}
