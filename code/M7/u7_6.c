#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_array(int n, int array[n]);
void make_experiment(int n, int hist[n], int throws);
void print_histogram(int n, const int hist[n]);
void graph_histogram(int n, const int hist[n]);
void (*printer) (int, const int*);

int main(void)
{
    const int n = 11;
    int hist[n];

    printer = print_histogram;

    srand(time(NULL));
    init_array(n, hist);
    make_experiment(n, hist, 1000);
    printer(n, hist);
    return 0;
}

void init_array(int n, int array[n])
{
    for (int i = 0; i < n; i++) {
        array[i] = 0;
    }
}

void make_experiment(int n, int hist[n], int throws)
{
    for (int i = 0; i < throws; i++) {
        int sum = rand() % 6 + 1 + rand() % 6 + 1;
        hist[sum-2]++;
    }
}

void graph_histogram(int n, const int hist[n])
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < hist[i]; j++) {
            printf("*");
        }
        printf("\n");
    }
}

void print_histogram(int n, const int hist[n])
{
    for (int i = 0; i < n; i++) {
        printf("%2d : %5d\n", i + 2, hist[i]);
    }
}
