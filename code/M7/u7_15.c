#include <stdio.h>

typedef struct {
    int a;
    double x;
} values;

typedef struct {
    values before;
    values after;
} change;

double f1(int n1, double n2)
{
    values v = {n1, n2};
    v.a = v.a + 5;
    v.x = v.x - 1;
    return v.a - v.x;
}

int f2(int n1, double n2)
{
    values v = {n1, n1 - n2};
    v.a = v.a + (int)v.x;
    v.x += v.a;
    return (int)v.x;
}

values f3(values current)
{
    values next;
    next.a = current.a * 2;
    next.x = current.x / 2;
    return next;
}

double f4(change change1)
{
    return change1.before.a * change1.before.x
        - change1.after.a * change1.after.x;
}

void f5(change *change2)
{
    (*change2).after = f3((*change2).before);
}

int main (void)
{

    // a
    printf("f1(3, 0.5) = %g\n", f1(3, 0.5));
    // b
    printf("f2(3, 0.5) = %d\n", f2(3, 0.5));
    // c
    values first = {5, 3.5};
    printf("f3(first) = {%d, %g}\n", (f3(first)).a, (f3(first)).x);
    // d
    change change1 = {{5, 3.5},{10, 1.75}};
    printf("f4(change1) = %g\n", f4(change1));
    // e
    change change2 = {{1, 2.5},{}};
    f5(&change2);
    printf("change2 = {{%d, %g}, {%d, %g}}\n",
           change2.before.a, change2.before.x, change2.after.a, change2.after.x);

    /* Test rounding behaviour when type casting */
    printf("(int) 2.5 = %d\n", (int) 2.5);
    printf("(int) 3.5 = %d\n", (int) 3.5);
    printf("(int) -2.5 = %d\n", (int) -2.5);
    printf("(int) -3.5 = %d\n", (int) -3.5);

    return 0;
}
