#include "mydate.h"
#include <stdio.h>

typedef struct {
    date startdate;
    date enddate;
    double avg_score;
    int number_of_participants;
} course_info;

date get_date(void);
void print_date(date d);
void print_course_info(course_info c);
course_info get_course_info(void);


int main (void)
{
    course_info ipc_vt24 = {
        .startdate = {
            .year = 2023,
            .month = 9,
            .day = 1},
        .enddate = {
            .year = 2024,
            .month = 1,
            .day = 15},
        .avg_score = 21.45,
        .number_of_participants = 252
    };

    ipc_vt24 = get_course_info();
    print_course_info(ipc_vt24);
        
    return 0;
}

date get_date(void)
{
    date date;
    printf("Enter number of year: ");
    scanf("%d", &date.year);
    printf("Enter number of month: ");
    scanf("%d", &date.month);
    printf("Enter number of day: ");
    scanf("%d", &date.day);

    return date;
}

void print_date(date d)
{
    printf("%02d%02d%02d\n", d.year % 100, d.month, d.day);
    
}

void print_course_info(course_info c)
{
    printf("\nStart date: ");
    print_date(c.startdate);
    printf("End date: ");
    print_date(c.enddate);
    printf("Average score: %g\n", c.avg_score);
    printf("Number of participants: %d\n\n", c.number_of_participants);
}

course_info get_course_info(void)
{
    course_info c;

    printf("\n--- Start date ---\n");
    c.startdate = get_date();
    printf("\n--- End date ---\n");
    c.enddate = get_date();
    printf("\nAverage score: ");
    scanf("%lf", &c.avg_score);
    printf("Number of participants: ");
    scanf("%d", &c.number_of_participants);

    return c;
}
