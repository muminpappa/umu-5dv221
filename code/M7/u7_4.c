#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    int correct_answer, guess, attempts = 0;
    
    srand(time(NULL));
    correct_answer = (rand() % 100) + 1;

    do {
        printf("Gissa talet: ");
        scanf("%d", &guess);
        attempts++;
        if (guess > correct_answer) {
            printf("Du gissade för högt!\n");
        }
        else if (guess < correct_answer) {
            printf("Du gissade för lågt!\n");
        } else {
            printf("Grattis, du gissade rätt!\n");
            printf("Du behövde %d försök\n", attempts);
        }
    } while ( guess != correct_answer);
    
    
    return 0;
}
