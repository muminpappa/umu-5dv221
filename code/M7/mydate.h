#ifndef MYDATE_H
#define MYDATE_H

typedef struct {
  int year;
  int month;
  int day;
} date;

#endif
