#include "mydate.h"
#include <stdio.h>

void print_date(date d);

int main (void)
{
    date date = {
        .year = 2024,
        .month = 2,
        .day = 26
    };

    print_date(date);
    
    return 0;
}

void print_date(date d)
{
    printf("%02d%02d%02d\n", d.year % 100, d.month, d.day);
    
}
