#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void){

    int lower, upper;

    printf("Ange det lägsta talet: ");
    scanf("%d", &lower);
    printf("Ange det största talet: ");
    scanf("%d", &upper);

    srand(time(NULL));

    for (int i = 0; i < 10; i++) {
        printf("%d\n", rand() % (upper - lower + 1) + lower);
    }

    return 0;
}
