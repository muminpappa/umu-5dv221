* object - objekt
* memory - minne
* typ - type
* size - omfång / storlek
* variable - variabel; namngivet objekt av en specifik typ
* identifier - identifierare; namnet på variabeln
* declaration - deklaration
* assignment - tilldelning
* initialization - initialisering
* integer - heltal
* value - värde
* constant - konstant
* floating point number - flyttal
* type casting - typkonvertering; implicit eller explicit
* expression - uttryck; består av operatorer och deras operander
* scope - räckvidd
* 
