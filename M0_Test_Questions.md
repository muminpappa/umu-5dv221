Imperativ programmering (C)

Modul 0

Komma igång

Testfrågor

Du anses ha bemästrat modulen när du förstår och kan utföra följande uppgifter.

1. Skapa en katalog vid namn alpha och inuti den en katalog beta. Navigera till
katalogen beta. Ta bort bägge katalogerna.

2. Logga in till en server på universitetet via SSH. Skapa en katalog vid namn alpha
och inuti den en katalog beta. Navigera till katalogen beta. Ta bort bägge katalo-
gerna.

3. Överför en lokal katalogstruktur till en server på universitetet. (Om du kollar på
https://support.cs.umu.se/intro/remote-files eller https://webapps.cs.umu.se/files/
ser du flera olika sätt som det kan göras.)

4. Installera och konfigurera en textredigerare, förslagsvis VS Code (Visual Studio
Code).

5. Spara följande C-kod i en fil med namnet hello.c. Kompilera och länka den till ett
exekverbart program. Kör det resulterande programmet och verifiera att det skriver
”Hello World!” till terminalen.

#include <stdio.h>

int main(void)
{

printf("Hello World !\n");

return 0;

}

1 av 1


