---
title: Imperativ programmering (C) Modul 7
---

# Enkla och sammansatta typer #

> Testfrågor
>
> OBS! Prova att deklarera strukturer både med och utan typedef.
>
> Du är redo för mästarprovet först när du förstår och kan svara på följande frågor.


> 1. Förklara varför flyttal är behäftade med avrundningsfel.

Utrymmet (i bitar) för att spara mantissan är begränsat.

> 2. Ange ett viktigt skäl till att det finns flera heltalstyper som
>    har olika stort omfång.

För att kunna väla det lämpliga omfånget för en viss användning utan
att slösa med minne.

> 3. Låt a och b vara positiva decimaltal. Förklara varför det är klokare att beräkna
> deras medelvärde
>
>         \frac{a+b}{2}
>
> med uttrycket a / 2 + b / 2 än med det mer naturliga uttrycket (a + b) / 2.
> Tips: tänk på det begränsade omfånget.

Med uttrycket `a/2 + b/2` kan man förhindra en overflow, eftersom
`a/2 + b/2 <= max(a,b)/2 + max(a,b)/2 = max(a,b)`, så om varken a
eller b överskrider omfånget för den valda typen så gör `a/2 + b/2`
det inte heller. Däremot kan `a + b` som beräknas först överstiga
omfånget för den valda typen.

> 4. Deklarera en strukturtyp som representerar ett datum som år, månad,
>    dag.

``` c
typedef struct {
    int year;
    int month;
    int day;
} date;
```

> 5. Deklarera en strukturtyp som representerar en person (förnamn,
> efternamn, födelsedatum).

``` c
typedef struct {
    char firstname[20];
    char lastname[20];
    date birthday;
} person;
```

> 6. Definiera följande funktion som räknar ut arean av en cirkel. Deklarera även den
> nämnda strukturtypen.
>
>         double area(const struct circle *circle );

``` c
typedef struct {
    double r;
} circle;

double area(const struct circle *circle )
{
    return 3.14159 * *circle.r * *circle.r;
}
```

> 7. Deklarera en strukturtyp struct rect som passar till följande kod:
>
>         struct rect r;
>         r.x[0] = 2;
>         r.x[1] = 3;
>         r.y[0] = 4;
>         r.y[1] = 5;

``` c
typedef struct {
    int x[4];
    int y[4];
} rect;
```

> 8. Givet deklarationen
>
>         struct card {
>         int suit;
>         int rank;
>         };
>
> skriv om följande kod så att den använder initialisering:
>
>         struct card card;
>         card.suit = 2;
>         card.rank = 9;

``` c
struct card card = {2, 9};
```

> 9. Givet deklarationerna
>
>         struct node {
>
>             int value;
>             struct node *children [2];
>
>         };
>
>         struct node parent;
>
> skriv ut värdet i parents andra barn.

``` c
printf("%d\n", parent.children[1]->value);
```

> 10. Vi vill representera n stycken punkter (xi, yi) i planet. Ett
> sätt är att lagra varje punkt i en struktur och sedan lagra
> strukturerna i en array av storlek n. Ett annat sätt är att lagra
> alla x-värden i en array av storlek n och alla y-värden i en annan
> array av storlek n och sedan lagra de två arrayerna i en
> struktur. Översätt bägge alternativen till deklarationer av typer
> och variabler.

Varje punkt i en struktur och sedan lagra strukturerna i en array av
storlek n:

``` c
typedef struct {
    double x;
    double y;
} point;

int n;
point points[n];
```

Lagra alla x-värden i en array av storlek n och alla y-värden i en
annan array av storlek n och sedan lagra de två arrayerna i en
struktur:

``` c
#define n 10

struct points {
    double x[n];
    double y[n];
}
```

> 11. Vad är det som är fel med följande funktionsdefinition? Antag
> att typen struct node är definierad och innehåller en medlem vid
> namn value.
>
>         struct node *create(int value)
>         {
>
>         struct node n = { .value = value };
>         return &n;
>
>         }

När `create()` avslutas så lämnas minnet tillbaka till
operativsystemet. Så det blir en dinglande pekare. Istället för en
pekare borde funktionen returnera själva posten.

> 12. Givet deklarationerna
>
>         typedef struct s {
>
>             double x;
>             int a[8];
>             char *b[3];
>             struct s *sp;
>
>         } s;
>
>         s s1;
>
> ange datatypen för följande uttryck:
>
> a)
>
>         s1

    s

> b)
>
>         s1.x

    double

>
> c)
>
>         s1.a

    pointer to int

>
> d)
>
>         s1.a[0]

    int

>
> e)
>
>         s1.b

    pointer to char

>
> f)
>
>         s1.b[2]

    char

>
> g)
>
>         s1.sp

    pointer to s

>
> h)
>
>         (*s1.sp).x

    double

> 13. Vad innebär det att flyttal är behäftade med avrundningsfel?

Man måste vara försiktig med jämförelser av typen `==` eller `!=`
mellan flyttal.

> 14. Låt a och b vara positiva decimaltal. Matematiskt är det
> aritmetiska uttrycket (a * b) / c ekvivalent med (a / c) * b. Så hur
> kommer det sig att motsvarande uttryck i språket C inte är
> ekvivalenta, dvs beroende på vilka värden som variablerna a, b och
> c har så kan det ena uttrycket vara korrekt medan det andra beräknar
> ett ogiltigt eller helt felaktigt värde.

Beroende på i vilken ordning beräkningarna utförs så kan det bli en
overflow (t ex vid beräkningen av `a * b` om a och b är mycket stora,
eller `a / c` om a är stor och c är mycket liten) eller underflow (om
a och b är mycket små.
