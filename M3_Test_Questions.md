---
title: Imperativ programmering (C) Modul 3
---

# Val-satser och logik #

> Testfrågor
>
> Du är redo för mästarprovet först när du förstår och kan svara på
> följande frågor.
>
> 1. Deklarera och initialisera boolska variabler med identifierarna
> sunny, windy och cold. Översätt följande påståenden till logiska
> uttryck:
>
> • Det är varmt och soligt (rätt svar: !cold && sunny).

`!cold && sunny`

> • Det är kallt och blåsigt.

`cold && windy`

> • Det är fint väder.

`cold && sunny`

> • Det är dåligt väder.

`!sunny`


> 2. Förenkla följande logiska uttryck:
>
> • !!!p

`!p`

> • p && !p

`false`

> • p || !p

`true`

> • false && p

`false`

> • false || p

`p`

> • p && (p || q)

`p`

> • (p || (q || (z)))

`p || q || z`

> 3. Förenkla följande uttryck (variablerna är av typen int):
>
> • a < 5 && a < 7

`a < 7`

> • a == 0 || a > -3

`a > -3`

> • a > b && b + 5 < a

`a > b + 5`

> • !(a > b)

`a <= b`

> 4. Ta bort en if-sats genom att utnyttja kortslutning av logiska
>    operatorer:

> if (p != NULL) {
>
> if (*p > 0) {
>
> // ...
>
> }
>
> }

``` c
if ( p != NULL && *p > 0){
// ...
}
```

> 5. Använd dig av att evalueringen av operatorn && är kortsluten
> (short circuited) för att skriva om följande kod så att den bara
> behöver en if-sats.

> if (a != 0) {
>
> if (!(b%a)) {
>
> x = b/a;
>
> }
>
> }

``` c
if (a != 0 && !( b % a)){
    x = b / a;
    }
```

> 6. Skriv om följande kod så den bara använder en if-sats.

> if (p) {
>
> if (!q) {
>
> if (z) {
>
> // ...
>
> }
>
> }
>
> }

``` c
if (p && !q && z){
    // ...
}
```

> 7. Skriv en funktion som returnerar talet 1 om det heltal som ges
> som första aktuella parameter är jämnt delbart med det tal som ges
> som andra aktuell parameter.  Annars ska talet 0 returneras. Din kod
> behöver bara hantera aktuella parametrar större än 0.

``` c
int divides(int a, int b)
{

    return !( a % b);

}
```

> 8. Skriv en funktion som tar tre aktuella parametrar av typen int
> och skriver ut det minsta värdet bland dem. Du måste i ditt svar
> använda nästlade if-satser och du får inte använda några
> variabeltilldelningar eller logiska operatorer.

``` c
int min_3(int a, int b, int c)
{
    if (a < b){
        if (a < c)
            return a;
        else
            return c;
        } else {
        if (b < c)
            return b;
        else
            return c;
        }
}
```

> 9. Skriv en funktion som skriver ut om det heltal som ges som
> aktuell parameter är ett jämnt tal eller ett udda tal. Utskriften
> ska se ut på följande sätt givet att funktionen anropas med talet
> -3:
>
> -3 is an odd number
>
> Utskriften ska se ut på följande sätt givet att funktionen anropas
> med talet 2:
>
> 2 is an even number

``` c
void check_number(int n)
{
    if ( n % 2 )
        printf("%d is an even number\n", n);
    else
        printf("%d is an odd number\n", n);
}
```

> 10. Definiera en funktion boom_bang som givet ett heltal n skriver ut
>     talet n förutom
>
> • om n är delbart med 3 skriv ut ”boom”,
>
> • om n är delbart med 5 skriv ut ”bang”,
>
> • om n är delbart med både 3 och 5 skriv ut ”boom bang”.
>
> Använd en eller flera if-satser.

``` c
void boom_bang(int n)
{

    if ( n % 15 == 0 )
    {
        printf("boom bang\n");
    } else if (n % 3 == 0) {
        printf("boom\n");
    } else if (n % 5 == 0) {
        printf("bang\n");
    } else {
        printf("%d\n", n);
}
```


> 11. Definiera en funktion boom_bang som givet ett heltal n skriver
>     ut talet n förutom
>
> • om n är delbart med 3 skriv ut ”boom”,
>
> • om n är delbart med 5 skriv ut ”bang”,
>
> • om n är delbart med både 3 och 5 skriv ut ”boom bang”.
>
> Använd en switch-sats.

``` c
void boom_bang(int n)
{
    switch ( n % 15 ){
        case 0: printf("boom bang\n"); break;
        case 3:
        case 6:
        case 9:
        case 12: printf("boom\n"); break;
        case 5:
        case 10: printf("bang\n"); break;
        default: printf("%d\n", n);
    }
```


> 12. Använd en switch-sats för att givet en variabel med värdet 0, 1,
> 2 eller 3 skriva ut Hjärter, Spader, Ruter respektive Klöver.

``` c
int n;
switch(n){
    case 0: printf("Hjärter\n"); break;
    case 1: printf("Spader\n"); break;
    case 2: printf("Ruter\n"); break;
    case 3: printf("Klöver\n"); break;
    default:
}
```

> 13. Använd en eller flera if-satser för att givet en variabel med
> värdet 0, 1, 2 eller 3 skriva ut Hjärter, Spader, Ruter respektive
> Klöver.

``` c
int n;
    if (n == 0)
        printf("Hjärter\n");
    else if (n == 1)
        printf("Spader\n");
    else if (n == 2)
        printf("Ruter\n");
    else if (n == 3)
        printf("Klöver\n");
```

> 14. Implementera den rekursiva matematiska funktionen
>
> f(n) =
>
> 1 om n = 1,
>
> 2 om n = 2,
>
> f(n − 2) + f(n − 1) om n > 2,
>
> som en rekursiv funktion i språket C.

``` c
int fib(int n)
{

    int rv;

    if (n == 1 || n == 2)
        rv = n;
    else if (n > 2)
        rv = fib(n - 2) + fib(n - 1);
    return rv;

}
```

> 15. Implementera den rekursiva matematiska funktionen
>
> f(n) = 1 om n = 1,
>
> n · f(n − 1) om n > 1,
>
> som en rekursiv funktion i språket C.

``` c
int factorial(int n)
{
    int rv;

    if (n == 1)
        rv = 1;
    else if (n > 1)
        rv = n * factorial(n - 1);

    return rv;
}
```

> 16. Översätt följande text till en rekursiv funktion i språket C.
>
> Talet 1 är ett udda tal men 2 är inte ett udda tal (utan ett jämnt tal).
> Ett positivt heltal n (större än 2) är ett udda tal om och endast om talet
> n − 2 är ett udda tal.

``` c
void even_or_odd(int n)
{

    if (n == 1)
        printf("Udd\n");
    else if (n == 2)
        printf("Jämt\n");
    else if (n > 2)
        even_or_odd(n - 2);

}
```

> 17. Skriv definitionen för en rekursiv funktion
>
>    int prod_pos_numbers(int a);
>
> som multiplicerar alla heltal från 1 till och med a, och returnerar
> resultatet. Om funktionen anropas med 0 eller ett negativt tal ska
> funktionen returnera 0.

``` c
int prod_pos_numbers(int a)
{
    int rv = 0;
    if (a == 1)
        rv = 1;
    else if (a > 1)
        rv = a * prod_pos_numbers(a - 1);
    return a;
}
```

> 18. Skriv definitionen av en funktion som returnerar summan av alla
> heltal från 1 till n där n ges som aktuell parameter till
> funktionen. Använd rekursion.

``` c
int cumsum(int n)
{
    int rv;
    if (n == 1)
        rv = 1
    else if (n > 1)
        rv = n + cumsum(n - 1);
    return rv;
}
```

> 19. Vad blir värdet av anropet rec(4) givet följande definition?

``` c
int rec(int n)
{
    if (n <= 2)
        return 1;
    else
        return n + rec(n - 1) + rec(n - 2);
}
```

```
rec(1)=1
rec(2)=1
rec(3)=3+1+1=5
rec(4)=4+5+1=10
```

> 20. Beskriv hur man prydligt formaterar och strukturerar en if-sats med både en
> if else-klausul och en else-klausul. Fokusera särskilt på placeringen av { och
> } samt indenteringen.
