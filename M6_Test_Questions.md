---
title: Imperativ programmering (C) Modul 6
---

# Arrayer och strängar #

> Testfrågor
>
> OBS! Använd const i alla funktionsdeklarationer där det är befogat.
>
> Du är redo för mästarprovet först när du förstår och kan svara på
> följande frågor.

> 1. Deklarera en array och initiera den med sekvensen 1, 2, 3, 4, 5.

``` c
int array[5] = {1, 2, 3, 4, 5};
```

> 2. Deklarera en array av char och initiera den med ditt namn.

``` c
char arrar[20] = "Axel Franke";
```

> 3. Skriv ut alla kommandoradsargument, ett per rad.

``` c
for (int i = 0; i < argc; i++){
    printf("%s\n", argv[i]);
    }
```

> 4. Skriv ett program som skriver ut hälsningen ”Hej Karin!” där
> ”Karin” anges via kommandoraden.

``` c
#include <stdio.h>

int main(int argc, char* argv[])
{
    if (argc == 2){
        printf("Hej %s!\n", argv[1]);
        }
    return 0;
}
```

> 5. Skriv en funktion som summerar alla tal i en array via iteration.

``` c
double sum_over_array(int n, const double array[])
{
    double sum = 0;
    for (int i = 0; i < n; i++){
        sum + = array[i];
    }
    return sum;
}
```

> 6. Skriv en funktion som summerar alla tal i en array via
> rekursion. Dela arrayen i två ungefär lika stora halvor och
> returnera summan av summorna av de två halvorna.  Beräkna summorna
> av halvorna via rekursion.

``` c
double sum_over_array(int n, const double array[])
{
    double sum;
    /* basfall */
    if (n == 1){
        sum = array[0];
    }
    else if (n == 2) {
        sum = array[0] + array[1];
    }
    /* rekursivt fall */
    else {
        sum = sum_over_array(n/2, array) + sum_over_array(n - n/2, &array[n/2]);
    }
    return sum;
}
```

Testat - det funkar.

> 7. Skriv en funktion som skriver över alla udda tal i en array
>    med 0.

``` c
void zero_odds(int n, int array[])
{
    for (int i = 0; i < n; i++){
        if (array[i] % 2){
            array[i] = 0;
        }
    }
}
```

> 8. Skriv en funktion som avgör om alla tal i en array är ensiffriga.

``` c
_Bool is_single_digit(int n, const int a[])
{
    _Bool single = 1;
    int i = 0;
    while (single && i < n){
        if (a[i] < -9 || a[i] > 9){
            single = 0;
        }
        i++;
    }
    return single;
}
```

> 9. Skriv en funktion som avgör om en array är sorterad i stigande
>    ordning.

``` c
_Bool is_sorted_ascended(int n, const double a[])
{
    _Bool sorted = 1;
    int i = 1;
    while (sorted && i < n){
        if (a[i] < a[i-1]){
            sorted = 0;
        }
        i++;
    }
    return sorted;
}
```

> 10. Skriv en funktion som beräknar längden på en sträng.

``` c
int strlen(int n, const char str[])
{
    int n = 0;

    while (str[n] != 0){
        n++;
    }

    return n;
}
```

> 11. Skriv en funktion som kopierar en sträng över en annan.

``` c
void copy_str(int n, const char src[], char dest[])
{
    int i = 0;
    do {
        dest[i] = src[i];
        i++;
    } while (src[i] != 0 && i < n);
}
```

> 12. Skriv en funktion som lägger till en sträng till slutet av en
>     annan.

``` c
void append_str(int n, const char src[], char dest[])
{
    int dest_end = 0, j = 0;
    /* Find end of string (\0) in dest */
    while (dest_end < n && dest[dest_end] != 0) {
        dest_end++;
    }
    /* copy characters one by one from src to dest */
    do {
        dest[dest_end + j] = src[j];
        j++;
    } while (dest_end + j < n && src[j] != 0);
}
```

> 13. Skriv en funktion som byter ut alla vokaler i en sträng mot ett
>     mellanrum.

``` c
void replace_vowels(int n, char str[])
{
    int i = 0;
    while (i < n && str[i] != 0){
        switch(str[i]){
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u': str[i] = ' '; break;
        }
        i++;
    }
}
```

> 14. Skriv en funktion som tar bort alla vokaler ur en sträng (och
>     därmed krymper den).

``` c
void delete_vowels(int n, char str[])
{
    int i = 0;
    while (i < n && str[i] != 0) {
        switch(str[i]){
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                str[i] = str[i+1];
                delete_vowels(n, str);
                break;
        }
    }
}
```

> 15. Skriv en funktion som skriver ut en textrad centrerat inom en
> viss kolumnbredd.  Exempel med en kolumnbredd på 20 (_ symboliserar
> mellanrum):
>
>         _____CENTRERAT!_____

``` c
void center_line(int n, const char str[])
{
    int length = 0, i;
    const int width = 20;
    char padding[width/2];
    while (length < n && str[length] != 0) {
        length++;
    }
    for (i = 0; i < (width - length)/2; i++){
        padding[i] = ' ';
    }
    padding[i] = 0;

    printf("%s%s%s\n", padding, str, padding);
}
```

> 16. Definiera en konstant SIZE med ett preprocessordirektiv, använd
> sedan den konstanten vid deklarationen av en tre-dimensionell array
> vars element är av typen ”pekare till char”.

``` c
#define SIZE 10

char *array[SIZE][SIZE][SIZE];
```


> 17. Vad är det som särskiljer en sträng från en array av char?

Strängen är terminerat med en null-byte (0, `'\0'`).


> 18. Betrakta följande kod:

    int foo(void)
    {
        int n = 3;
        int arr[] = {7, 2, 5};

        for (int i = 1 ; i < n ; i++ ) {
            for (int j = 1 ; j < n ; j++) {
                if (arr[j-1] < arr[j]) {
                    int temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr[2];
    }

> Vilka returvärde ger ett anrop till foo? Börja med att handevaluera koden noggrant
> på papper. Därefter kan du skriva in den och testköra.

    i  j  arr[j-1] < arr[j]   tmp  arr[0]  arr[1] arr[2]
                                   7       2      5
    1  1  false
    1  2  true                2    7       5      2
    2  1  false
    2  2  false

`arr[2] = 2`

> 19. Skriv en funktion som tar en två-dimensionell array av int som
> argument (samt argument för de två dimensionernas storlekar) och
> returnerar medelvärdet av de ingående elementen. Kom ihåg att
> använda const där det är befogat. Testkör din kod flera gånger så
> att du är säker på att den räknar rätt.

``` c
double mean_of_matrix(int rows, int columns, int array[][])
{
    double sum = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            sum += (double) array[i][j];
        }
    }

    return sum / (rows * columns);
}
```

> 20. Skriv en funktion som skriver ut innehållet i en sträng
> baklänges, ett tecken i taget.  Funktionen ska ha endast en formell
> parameter av typen pekare till char. Tänk på att använda dig av
> ”slutet-på-strängen”-tecknet. Kom ihåg att använda const där det är
> befogat.

``` c
void print_backwards(char str[])
{
    int n = 0;
    while (str[n]) {
        n++;
    }
    for (int i = n - 1; i >= 0; i--) {
        printf("%c\n", str[i]);
    }
}
```
